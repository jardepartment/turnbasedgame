// ClientSocket.js 
//    - Main entry point of the front end. holds the websocket ('socket') which is used to 
//      send/receive data to/from the server. 
//    - Also responsible for forwarding messages received to the corresponding message
//      handlers based on the state of the client. See 'changeStates' function.

clientState = "initial-state";
var factionCanvas = document.getElementById("faction-select-canvas");
factionCanvas.width = 108 * 4;
factionCanvas.height = 108;
var factionCtx = factionCanvas.getContext("2d");


var usernameInput = document.getElementById("username-input");

socket = new WebSocket("ws://localhost:4321/tbg/endpoint");

socket.onopen = function(message){
	console.log(message);
}

socket.onclose = function(message){
	console.log(message);
	location.reload();
	socket.close();
}

socket.onerror = function(message){
	console.log(message);
}

socket.onmessage = function(message){
	switch(clientState){
		case "main-menu-state":
			mainMenuMessageHandler(message);
			break;
		case "lobby-browser-state":
			lobbyBrowserMessageHandler(message);
			break;
		case "map-create-state":
			mapCreateMessageHandler(message);
			break;
		case "lobby-state":
			lobbyMessageHandler(message);
			break;
		case "in-battle-state":
			battleMessageHandler(message);
			break;
		case "initial-state":
			initialStateMessageHandler(message);
			break
		default:
			console.log("unknown client state, message dropped: " + message);
	}
}

document.getElementById("faction-radio-group").addEventListener('change', function(){
	sendFactionSelectUpdate();
	drawFactionSelect();
});

function browseLobbies(){
	var browseLobbyMessage = '{ "message_type" : "lobby", "message_id" : "browse_lobbies", "lobby_type" : "open" }';
	socket.send(browseLobbyMessage);
	changeStates("lobby-browser-state");
}

function changeStates(stateId){
	var elements = document.getElementsByClassName("client-state");

    for (var i = 0; i < elements.length; i++){
        elements[i].style.display = "none";
    }
    
    if (stateId != "initial-state"){
    	document.getElementById("nav-bar").style.display = "block";
    	if(stateId != "main-menu-state"){
    		document.getElementById("faction-radio-group").disabled = true;
    	}else{
    		document.getElementById("faction-radio-group").disabled = false;
    	}
    }else{
    	document.getElementById("nav-bar").style.display = "none";
    }
    
    clientState = stateId;
    document.getElementById(stateId).style.display = "inline-block";
}

function createMap(){
	setResultingMapSize();
	changeStates("map-create-state");
	initializeMapCreate(usernameInput.value);
}

function createNewLobby(map){
	changeStates("lobby-state");
	if(map == null){
		socket.send('{ "message_type" : "lobby", "message_id" : "create_lobby" }');
	}else{
		socket.send('{ "message_type" : "lobby", "message_id" : "create_lobby", "map" : ' + JSON.stringify(map) + ' }');
	}
}

function disconnect(){
	socket.close();//tell the server we left
	location.reload();//refresh page, establishes new connection
}

function drawFactionSelect(factionString){
	var factionString =  $("input[name='faction-radio-group']:checked").val();
	document.getElementById("faction-select-button").innerHTML = username + " | <i>" + factionString + "</i>";
	factionString = factionString.toLowerCase();
	var leaderImage = document.getElementById(factionString+"-leader-sprite");
	var meleeImage = document.getElementById(factionString+"-melee-sprite");
	var rangedImage = document.getElementById(factionString+"-ranged-sprite");
	var mountedImage = document.getElementById(factionString+"-mounted-sprite");
	
	factionCtx.clearRect(0,0,factionCanvas.width,factionCanvas.height);
	factionCtx.fillStyle = '#45283c50';
	factionCtx.fillRect(0,0,factionCanvas.width,factionCanvas.height);
	factionCtx.drawImage(leaderImage, // source image
            0,      // start of x on source
            0,      // start of y on source
            108,    // width to take from source
            108,    // height to take from source
            0,      // start x on canvas to draw
            0,      // start y on canvas to draw
            108,    // width on canvas
            108);   // height on canvas
	
	factionCtx.drawImage(meleeImage, // source image
			0,      // start of x on source
            0,      // start of y on source
            108,    // width to take from source
            108,    // height to take from source
            108,    // start x on canvas to draw
            0,      // start y on canvas to draw
            108,    // width on canvas
            108);   // height on canvas
	
	factionCtx.drawImage(rangedImage, // source image
			0,      // start of x on source
            0,      // start of y on source
            108,    // width to take from source
            108,    // height to take from source
            2*108,  // start x on canvas to draw
            0,      // start y on canvas to draw
            108,    // width on canvas
            108);   // height on canvas

	factionCtx.drawImage(mountedImage, // source image
			0,      // start of x on source
            0,      // start of y on source
            108,    // width to take from source
            108,    // height to take from source
            3*108,  // start x on canvas to draw
            0,      // start y on canvas to draw
            108,    // width on canvas
            108);   // height on canvas
}

function goToHome(){
	if(clientState == "lobby-state"){
		socket.send('{ "message_type" : "lobby", "message_id" : "leave_lobby" }');
	}
	
	if(clientState == "in-battle-state"){
		socket.send('{ "message_type" : "battle", "battle_id" : ' + battleId + ', "message_id" : "player_left_battle" }');
	}
	
	changeStates("main-menu-state");
}

function initialStateMessageHandler(message){
	var response = JSON.parse(message.data);
	switch(response.message_id){
		case "login_response":
			if(response.is_successful){
				changeStates("main-menu-state");
				username = response.username;
				drawFactionSelect();
			}else{
				alert("Non-unique session ID");
			}
			break;
		case "player_count_broadcast":
			updatePlayerCount(response.count);
			break;
		case "unit_config":
			updateJarpedia(response.config);
			break;
		default:
			console.log("unknown message received in initialStateMessageHandler:" + response.message_id);
	}
}

function login(){
	if(usernameInput.value.trim().length == 0){
		usernameInput.value = '';
		usernameInput.placeholder = "Enter a username...";
	}else{
		var loginMessage = '{ "message_type" : "lobby", "message_id" : "user_login", "username" : "' + usernameInput.value + '" }';
		socket.send(loginMessage);
		sendFactionSelectUpdate();
	}
}

function mainMenuMessageHandler(message){
	var mainMenuMessage = JSON.parse(message.data);
	var messageId = mainMenuMessage.message_id;
	switch(messageId){
		case "player_count_broadcast":
			updatePlayerCount(mainMenuMessage.count);
			break;
		case "battle_over":
			// no action needed, you left the battle (disconnect/forfeit)
			break;
		default:
			console.log("mainMenuMessageHandler received unknown message");
			console.log(mainMenuMessage);
	}
}

function sendFactionSelectUpdate(){
	var factionIdSelected = $("input[name='faction-radio-group']:checked").val().toLowerCase()
	socket.send('{ "message_type" : "lobby", "message_id" : "faction_selected", "faction" : "' + factionIdSelected + '" }');
}

function updateJarpedia(unitConfig){
	document.getElementById("jarpedia-body").innerHTML = JSON.stringify(unitConfig);
}

function updatePlayerCount(count){
	document.getElementById("current-player-count").innerHTML = "Players online: " + count;
}
