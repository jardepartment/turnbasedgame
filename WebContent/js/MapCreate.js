// MapCreate.js 
//    - Contains functions used by the map create tool ("map-create-state"). 

var mapWidthDiv = document.getElementById("map-width-input");
var mapHeightDiv = document.getElementById("map-height-input");
var alteringTileType = document.getElementById("altering-tile-type-dropdown");
var alteringTileElevation = document.getElementById("altering-tile-elevation-select");
var symmetryRadioGroup = document.getElementById("symmetry-radio-group");
var alteringTileType = document.getElementById("altering-tile-type-dropdown");
var DEFAULT_TILE = { "type" : "grass", "elev" : 1 };
var tiles = [];
var mapWidth = parseInt(mapWidthDiv.value);
var mapHeight = parseInt(mapHeightDiv.value);
var objX = Math.floor(mapWidth/2);
var objY = Math.floor(mapHeight/2);
var FPS_DELAY = 40;
var focusX = 0;
var focusY = 0;
var mapChanged = true;

setInterval(function(){
	if(clientState != "map-create-state"){
		return;
	}
	
	if(mapChanged){
		redrawMapCanvas(focusX, focusY);
		mapChanged = false;
	}
	
	//TODO no need to redraw, might use later for information display, bulk edit operations?
	//redrawUiCanvas(focusX, focusY);
}, FPS_DELAY);

document.getElementById("map-create-ui-ctx").oncontextmenu = function(event){
	setSelectedTile(undefined, undefined);
}

function addColumn(){
	for(var i = mapHeight; i > 0; i--){
		var insertIndex = i*mapWidth;
		tiles.splice(insertIndex, 0, DEFAULT_TILE);
	}
}

function addRow(){
	for(var i = 0; i < mapWidth; i++){
		tiles.push(DEFAULT_TILE);
	}
}

function alterTiles(selectedCoordinate, symmetry){
	var tilesToModify = [];
	tilesToModify.push({"x" : selectedCoordinate.x, "y" : selectedCoordinate.y});
	if(symmetry == "both"){
		var mirroredHorizontal = getMirroredHorizontalTile(selectedCoordinate.x, selectedCoordinate.y);
		tilesToModify.push(getMirroredVerticalTile(selectedCoordinate.x, selectedCoordinate.y));
		tilesToModify.push(getMirroredVerticalTile(mirroredHorizontal.x, mirroredHorizontal.y));
		tilesToModify.push(mirroredHorizontal);
	}else if(symmetry == "vertical"){
		tilesToModify.push(getMirroredVerticalTile(selectedCoordinate.x, selectedCoordinate.y));
	}else if(symmetry == "horizontal"){
		tilesToModify.push(getMirroredHorizontalTile(selectedCoordinate.x, selectedCoordinate.y));
	}else if(symmetry == "diagonal"){
		var mirroredHorizontal = getMirroredHorizontalTile(selectedCoordinate.x, selectedCoordinate.y);
		tilesToModify.push(getMirroredVerticalTile(mirroredHorizontal.x, mirroredHorizontal.y));
	}
	
	for(var i in tilesToModify){
		performTileAlteration(tilesToModify[i].x, tilesToModify[i].y);
	}
}

function changeDimensions(){
	if((mapWidthDiv.value == "") || (mapHeightDiv.value == "")){
		return;
	}
	
	var parsedWidth = parseInt(mapWidthDiv.value);
	var parsedHeight = parseInt(mapHeightDiv.value);
	
	if((parsedWidth < parseInt(mapWidthDiv.min)) ||
		(parsedWidth > parseInt(mapWidthDiv.max)) ||
		(parsedHeight < parseInt(mapHeightDiv.min)) ||
		(parsedHeight > parseInt(mapHeightDiv.max))){
		return;
	}
	
	if(mapWidth < parsedWidth){
		while(mapWidth < parsedWidth){
			addColumn();
			mapWidth++;
		}
	}else if(mapWidth > parsedWidth){
		while(mapWidth > parsedWidth){
			removeColumn();
			mapWidth--;
		}
	}
	
	if(mapHeight < parsedHeight){
		while(mapHeight < parsedHeight){
			addRow();
			mapHeight++;
		}
	}else if(mapHeight > parsedHeight){
		while(mapHeight > parsedHeight){
			removeRow();
			mapHeight--;
		}
	}
	
	objX = Math.floor(mapWidth/2);
	objY = Math.floor(mapHeight/2);
	
	setMapJson({ "width" : mapWidth, "height" : mapHeight, "objective_coordinate_x" : objX, "objective_coordinate_y" : objY, "tiles" : tiles });
	setResultingMapSize(mapWidth, mapHeight);
	mapChanged = true;
}

function getMirroredHorizontalTile(x, y){
	return {"x":x, "y":(mapHeight-(y+1))}
}

function getMirroredVerticalTile(x, y){
	return {"x":mapWidth-(x+1), "y":y}
}

function hostLobbyWithCreatedMap(){
	alert("operation not supported yet!");//TODO
	//createNewLobby({ "width" : mapWidth, "height" : mapHeight, "objective_coordinate_x" : objX, "objective_coordinate_y" : objY, "tiles" : tiles });
}

function initializeMapCreate(){
	mapChanged = true;
	tiles = [];
	for(var i=0; i<mapWidth*mapHeight; i++){
		tiles.push(DEFAULT_TILE);
	}
	
	initializeGraphics({ "width" : mapWidth, "height" : mapHeight, "objective_coordinate_x" : objX, "objective_coordinate_y" : objY, "tiles" : tiles }, "map-create-ctx", null, "map-create-ui-ctx", null, false);
}

function mapCreateMessageHandler(message){
	var mapCreateMessage = JSON.parse(message.data);
	var messageId = mapCreateMessage.message_id;
	switch(messageId){
		case "save_map_response":
			if(mapCreateMessage.response.success){
				changeStates("main-menu-state");
			}else{
				alert(mapCreateMessage.response.message);
			}
			break;
		default:
			console.log("mapCreateMessageHandler received unknown message");
			console.log(mapCreateMessage);
	}
}

function performTileAlteration(xCoord, yCoord){
	var parsedElevation = parseInt(alteringTileElevation.value);
	if((parsedElevation > parseInt(alteringTileElevation.max)) ||
		(parsedElevation < parseInt(alteringTileElevation.min))){
		return;
	}
	
	var alteringTile = {"type" : alteringTileType.value, "elev" :  parsedElevation};
	tiles.splice(((yCoord*mapWidth)+xCoord), 1, alteringTile);
	
	mapChanged = true;
}

function removeColumn(){
	for(var i = mapHeight; i > 0; i--){
		var insertIndex = i*mapWidth;
		tiles.splice(insertIndex-1, 1);
	}
}

function removeRow(){
	tiles.splice((mapWidth*mapHeight)-mapWidth, mapWidth);
}

function saveMap(){
	var map = JSON.stringify({ "width" : mapWidth, "height" : mapHeight, "objective_coordinate_x" : objX, "objective_coordinate_y" : objY,  "tiles" : tiles });
	var saveMapMessage = '{ "message_type" : "lobby", "message_id" : "save_map", "map" : ' + map + ' }';
	socket.send(saveMapMessage);
}

function setResultingMapSize(){
	var area = mapWidthDiv.value*mapHeightDiv.value;
	var mapSize = "";
	
	// copied from Grid.java
	var TILE_AREA_SMALL_UB = 13*9;
	var TILE_AREA_MEDIUM_LB = 13*8;
	var TILE_AREA_MEDIUM_UB = 16*11;
	var TILE_AREA_LARGE_LB = 15*10;
	var TILE_AREA_LARGE_UB = 21*15;
	var TILE_AREA_X_LARGE_LB = 20*15;
	
	if (area < TILE_AREA_MEDIUM_LB) {
		mapSize = "Small";
	} else if (area <= TILE_AREA_SMALL_UB) {
		mapSize = "Small/Medium";
	} else if (area < TILE_AREA_LARGE_LB) {
		mapSize = "Medium";
	} else if (area <= TILE_AREA_MEDIUM_UB) {
		mapSize = "Medium/Large";
	} else if (area < TILE_AREA_X_LARGE_LB) {
		mapSize = "Large";
	} else if (area <= TILE_AREA_LARGE_UB){
		mapSize = "Large/X-Large";
	} else {
		mapSize = "X-Large";
	}
	
	document.getElementById("resulting-map-size").innerHTML = "Resulting map size: " + mapSize;
}

//******************************************************************************************
//TODO below should move to a Ui.js file and copied code in Battle.js should be removed.
//******************************************************************************************

var clickAndDragDeadZone = 5;
var clickingMouse1 = false;
var movingMap = false;
var lastClickedX = 0;
var lastClickedY = 0;
var lastOffsetX = 0;
var lastOffsetY = 0;

var $uiCtx = $('#map-create-ui-ctx');
$uiCtx.on('mousemove', function handler(event) {
    if (clickingMouse1 && ((Math.abs(event.offsetX-lastOffsetX) > clickAndDragDeadZone) || 
    					   (Math.abs(event.offsetY-lastOffsetY) > clickAndDragDeadZone) || 
    					    movingMap)) {
    	focusX = lastClickedX-event.offsetX;
    	focusY = lastClickedY-event.offsetY;
    	movingMap = true;
    	mapChanged = true;
    }
});

$uiCtx.on('mousedown', function(event){
	lastClickedX = event.offsetX+focusX;
	lastClickedY = event.offsetY+focusY;
	lastOffsetX = event.offsetX;
	lastOffsetY = event.offsetY;
	clickingMouse1 = true;
});

$uiCtx.on('mouseup', function(event){
	if(!movingMap){
		var selectedCoord = getSelectedCoordinate(event.offsetX, event.offsetY, focusX, focusY);
		if(isCoordinateOnMap(selectedCoord)){
			alterTiles(selectedCoord, symmetryRadioGroup.elements["symmetry-radio"].value);
			setSelectedTile(selectedCoord.x, selectedCoord.y);
		}
	}
	
	clickingMouse1 = false;
	movingMap = false;
});

$uiCtx.on('mouseleave', function(event){
	clickingMouse1 = false;
	movingMap = false;
});

$uiCtx.on('wheel',function(event){
	if (event.originalEvent.deltaY < 0) {
		mapZoom(true);
	}
	
	if (event.originalEvent.deltaY > 0) {
		mapZoom(false);
	}
	
	mapChanged = true;
    event.preventDefault();
});

function isCoordinateOnMap(coordinate){
	return ((coordinate.x < mapWidth) && (coordinate.y < mapHeight) &&
			   (coordinate.x >= 0) && (coordinate.y >= 0));
}
