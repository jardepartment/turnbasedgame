// Player.js 
//    - Contains functions/variables associated with the notion of a player/ui, mostly specific to the "in-battle-state". 

isPlayerTurn = false;
isLeftSidePlayer = true;
username = '';

var selectedTileX = "";
var selectedTileY = "";

$("table").delegate('tr','mousedown', function(e) {
	var currentRow=$(this).closest("tr"); 
	var x = parseInt(currentRow.find("td:eq(1)").text());
	if (x >= 0){
		highlightUnitTable(parseInt(currentRow.find("td:eq(0)")));
		selectedTileX = x;
		selectedTileY = parseInt(currentRow.find("td:eq(2)").text());
	}
});

function addUnitToTable(tableId, unit, canMove, canPerformAction){
	
	var tableRow = document.createElement('tr');
	tableRow.className = "unit-list-row";
	tableRow.onclick = function(){
		var message = {};
		if(inSetupState){
			var message = '{ "message_type" : "battle", "message_id" : "inventory_select", "unit_id" : '+ unit.id + ', "battle_id" : ' + battleId+ '}';
		}else{
			var message = '{ "message_type" : "battle", "message_id" : "tile_select", "battle_id" : ' + battleId + ', "x" : ' + unit.x+', "y" : ' + unit.y+'}';
		}
		
		socket.send(message);
		selectUnit(unit);
		highlightUnitTable(unit.id);
	}
	
	var id = document.createElement('td');
	var x = document.createElement('td');
	var y = document.createElement('td');
	
	id.style.display = "none";
	x.style.display = "none";
	y.style.display = "none";
	
	id.append(unit.id);
	x.append(unit.x);
	y.append(unit.y);
	
	tableRow.append(id);
	tableRow.append(x);
	tableRow.append(y);
	tableRow.append(unit.name);
	
	if (canMove){
		var moveIcon = document.createElement('div');
		moveIcon.disabled = true;
		moveIcon.title = "can move";
		moveIcon.className="unit-list-icon glyphicon glyphicon-share-alt";
		tableRow.append(moveIcon);
	}
	
	if (canPerformAction){
		var actionIcon = document.createElement('div');
		actionIcon.disabled = true;
		if(unit.unit_class != "leader"){
			actionIcon.title = "can attack";
			actionIcon.className="unit-list-icon glyphicon glyphicon-screenshot";
		}else{
			actionIcon.title = "can heal";
			actionIcon.className="unit-list-icon glyphicon glyphicon-plus-sign";
		}
		
		tableRow.append(actionIcon);
	}
	
	$(tableId).append(tableRow);
}

function clearSelectedAction(){
	selectedTileX = "";
	selectedTileY = "";
	var str;
	if(isPlayerTurn){
		str = "It is your turn!";
	}else{
		str = "waiting...";
	}
	
	document.getElementById("battle-state-label").innerHTML = str;
}

function endTurn(){
	var endTurnMessage = '{ "message_type" : "battle", "message_id" : "end_turn", "battle_id" : ' + battleId + '}';
	console.log(endTurnMessage);
	socket.send(endTurnMessage);
}

function getSelectedTileX(){
	return selectedTileX;
}

function getSelectedTileY(){
	return selectedTileY;
}

function highlightUnitTable(unitId){
	$("#own-units-table tr").each(function () {
		id = parseInt($(this).find("td:eq(0)").text());
        if (id == unitId) {
            $(this).css("border-style", "inset");
        } else {
        	$(this).css("border-style", "none");
        }
    });
	
	$("#enemy-units-table tr").each(function () {
		id = parseInt($(this).find("td:eq(0)").text());
        if (id == unitId) {
            $(this).css("border-style", "inset");
        } else {
        	$(this).css("border-style", "none");
        }
    });
}

function performQuickAction(x, y, action){
	var actionMessage = '{ "message_type" : "battle", "message_id" : "' + action + '", "battle_id" : ' + battleId + ', "x" : ' + x + ', "y" : ' + y + ', "selected_unit_id":' + selectedUnitId +'}';
	
	socket.send(actionMessage);
}

function performTileSelect(x, y){
	setSelectedTile(x, y);
	var actionMessage = '{ "message_type" : "battle", "message_id" : "tile_select", "battle_id" : ' + battleId + ', "x" : ' + x + ', "y" : ' + y + '}';
	socket.send(actionMessage);
	console.log(actionMessage);
}

function playSelectionAudio(actionId){
	//TODO handle audio depending on action
	if(actionId == "attack"){
		document.getElementById("sword_select_audio").play();
	}
}

function refreshEnemyUnitList(enemyUnitsJsonArray){
	$("#enemy-units-table").empty();
	console.log(enemyUnitsJsonArray);
	
	for(var i = 0; i<enemyUnitsJsonArray.length; i++){
		let enemyUnit = enemyUnitsJsonArray[i];
		if(!enemyUnit.is_alive){
			continue;
		}

		addUnitToTable("#enemy-units-table", enemyUnit, false, false);
	}
}

function refreshOwnUnits(unitJsonArray, canMove, canAttack){
	var tableId = "#own-units-table";
	$(tableId).empty();//reset inventory div
	
	for(var i = 0; i < unitJsonArray.length; i++){
		let unit = unitJsonArray[i];
		if(!unit.is_alive){
			continue;
		}

		if (inSetupState){
			addUnitToTable(tableId, unit, true, false);
		}else{
			if(canMove && canAttack){
				addUnitToTable(tableId, unit, !unit.has_moved, !unit.has_performed_action);
			}else if(canMove){// moves only
				addUnitToTable(tableId, unit, !unit.has_moved, false);
			}else if(canAttack){// attacks only
				addUnitToTable(tableId, unit, false, !unit.has_performed_action);
			}else{
				addUnitToTable(tableId, unit, false, false);
			}
		}
	}
}

function selectTile(tile){
	$("#advanced-tile-info-table").empty();
	$("#advanced-tile-info-table").prepend("<caption style=\"text-align:center;\">Environment</caption>");
	$("#advanced-tile-info-table").append("<tr title=\"Elevation of the currently selected tile.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-stats\"></div> Elevation</td><td style=\"float:right; padding-right:15px;\">"+tile.elev+"</td></tr>");
	$("#advanced-tile-info-table").append("<tr title=\"Type of the currently selected tile.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-asterisk\"></div> Type</td><td style=\"float:right; padding-right:15px;\">"+tile.type+"</td></tr>");
}

function selectUnit(unit){
	selectedUnitId = unit.id;
	updateInfoTable(unit);
	if (unit.x >= 0){
		setSelectedTile(unit.x, unit.y);
		highlightUnitTable(unit.id);
	}
}

function setSelectedTile(x, y){
	selectedTileX = x;
	selectedTileY = y;
}
