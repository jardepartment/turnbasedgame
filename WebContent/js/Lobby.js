// Lobby.js 
//    - Contains functions used in the "lobby-state". 

var lobbyId = null;

var leftVotedMap = null;
var rightVotedMap = null;

var isLeftVoteRandom = false;
var isRightVoteRandom = false;

var leftVoteCanvas = document.getElementById("left-map-vote-canvas");
var leftVoteCtx = leftVoteCanvas.getContext("2d");

var rightVoteCanvas = document.getElementById("right-map-vote-canvas");
var rightVoteCtx = rightVoteCanvas.getContext("2d");

function drawMapVotes(){
	leftVoteCanvas.width = 300;
	leftVoteCanvas.height = 150;
	rightVoteCanvas.width = 300;
	rightVoteCanvas.height = 150;
	
	leftVoteCtx.clearRect(0,0,leftVoteCanvas.width, leftVoteCanvas.height);
	rightVoteCtx.clearRect(0,0,rightVoteCanvas.width, rightVoteCanvas.height);

	if (isLeftVoteRandom){
		leftVoteCtx.drawImage(document.getElementById("question-mark"), leftVoteCanvas.width/2 - 72, 0);
	}else if (leftVotedMap == null){
		leftVoteCtx.drawImage(document.getElementById("empty-set"), leftVoteCanvas.width/2 - 72, 0);
	}else{
		drawMinimap(leftVoteCanvas, leftVotedMap);
	}
	
	if (isRightVoteRandom){
		rightVoteCtx.drawImage(document.getElementById("question-mark"), rightVoteCanvas.width/2 - 72, 0);
	}else if (rightVotedMap == null){
		rightVoteCtx.drawImage(document.getElementById("empty-set"), rightVoteCanvas.width/2 - 72, 0);
	}else{
		drawMinimap(rightVoteCanvas, rightVotedMap);
	}
}

function drawMinimap(canvas, map){
	var MINIMAP_TILE_WIDTH = 16;
	var mapCtx = canvas.getContext("2d");
	var mapWidth = map.width;
	var mapHeight = map.height;
	canvas.width = MINIMAP_TILE_WIDTH*mapWidth;
	canvas.height = MINIMAP_TILE_WIDTH*mapHeight;
	for(var row = 0; row < mapHeight; row++){
		for(var column = 0; column < mapWidth; column++){
			var tileIndex = (row*mapWidth) + column;
			var startingX = (column*MINIMAP_TILE_WIDTH);
			var startingY = (row*MINIMAP_TILE_WIDTH);
			mapCtx.strokeRect(startingX, startingY, MINIMAP_TILE_WIDTH, MINIMAP_TILE_WIDTH);
			mapCtx.fillStyle = get2dMapTileColor(map.tiles[tileIndex], true);
			mapCtx.fillRect(startingX, startingY, MINIMAP_TILE_WIDTH, MINIMAP_TILE_WIDTH);
			if (map.tiles[tileIndex].type == "trees"){
				
				mapCtx.drawImage(document.getElementById("trees-overlay"),// source image
		                        0, // start of x on source
		                        0, // start of y on source
		                        144,                  // width to take from source
		                        144,                  // height to take from source
		                        startingX,            // start x on canvas to draw
		                        startingY, // start y on canvas to draw
		                        MINIMAP_TILE_WIDTH,               // width on canvas
		                        MINIMAP_TILE_WIDTH);              // height on canvas
				
			}
			
			if (map.objective_coordinate_x == column && map.objective_coordinate_y == row){
				mapCtx.drawImage(document.getElementById("objective-overlay-2d"),// source image
                        0, // start of x on source
                        0, // start of y on source
                        144,                  // width to take from source
                        144,                  // height to take from source
                        startingX,            // start x on canvas to draw
                        startingY, // start y on canvas to draw
                        MINIMAP_TILE_WIDTH,               // width on canvas
                        MINIMAP_TILE_WIDTH);              // height on canvas
			}
			
			mapCtx.fillStyle = "white";
			var boxWidth = 100;
			var boxHeight = 14;
			var boxPadding = 3;
			mapCtx.fillRect(canvas.width-boxWidth-boxPadding, canvas.height-boxHeight-boxPadding, boxWidth, boxHeight);
			mapCtx.fillStyle = "rgba(0,0,0,1)";
			mapCtx.font = 'italic 12px Arial';
			mapCtx.fillText(map.creator, canvas.width-boxWidth-boxPadding+1, canvas.height-boxPadding-1);
		}
	}
}

function handleMapPicked(leftMapPicked){
	var chosenCtx = leftMapPicked ? leftVoteCtx : rightVoteCtx;
	var chosenCanvas = leftMapPicked ? leftVoteCanvas : rightVoteCanvas;

	var eliminatedCtx = leftMapPicked ? rightVoteCtx : leftVoteCtx;
	var eliminatedCanvas = leftMapPicked ? rightVoteCanvas : leftVoteCanvas;
	
	var alpha = 0.0, 
	mapFadeInterval = setInterval(function() {
		eliminatedCtx.fillStyle = "rgba(0, 0, 0, " + alpha + ")";
		eliminatedCtx.fillRect(0, 0, eliminatedCanvas.width, eliminatedCanvas.height);
		
		chosenCtx.strokeStyle = "rgba(255, 255, 0, " + alpha + ")";
		chosenCtx.lineWidth = 15;
		chosenCtx.strokeRect(0, 0, chosenCanvas.width, chosenCanvas.height);
		
		alpha = alpha + 0.01;
		if(alpha >= 0.5){
			clearInterval(mapFadeInterval);
		}
	}, FPS_DELAY*2);
}

function handleMapVoteBroadcast(lobbyMessage){
	if(lobbyMessage.left_map != null){
		if(lobbyMessage.left_map == "random"){
			isLeftVoteRandom = true;
		}else{
			isLeftVoteRandom = false;
			leftVotedMap = lobbyMessage.left_map;
		}
	}
	
	if(lobbyMessage.right_map != null){
		if(lobbyMessage.right_map == "random"){
			isRightVoteRandom = true;
		}else{
			isRightVoteRandom = false;
			rightVotedMap = lobbyMessage.right_map;
		}
	}
	
	drawMapVotes();
}

function lobbyMessageHandler(message){
	var lobbyMessage = JSON.parse(message.data);
	var messageId = lobbyMessage.message_id;
	switch(messageId){
		case "failed_to_add":
			alert("Could not join lobby.\n\nReason: " + lobbyMessage.reason);
			changeStates("main-menu-state");
			break;
		case "lobby_member_update":
			refreshLobbyList(lobbyMessage)
			break;
		case "map_vote_broadcast":
			handleMapVoteBroadcast(lobbyMessage);
			break;
		case "map_search_results":
			refreshMapOptionsTable(lobbyMessage.maps);
			console.log(lobbyMessage.maps);
			break;
		case "map_picked":
			handleMapPicked(lobbyMessage.left_map_picked);
			break;
		case "battle_map_init":
			changeStates("in-battle-state");
			//forward message to new handler
			battleMessageHandler(message);
			break;
		case "game_starting_countdown":
			updateLobbyHeader("Game starting in " + lobbyMessage.seconds_left + "..");
			break;
		case "join_lobby_response":
		case "player_count_broadcast":
			// ignore
			break;
		default:
			console.log("lobbyMessageHandler received unknown message");
			console.log(lobbyMessage);
	}
}

function mapSearch(){
	var sizeSearchOption = document.getElementById("map-search-size").value;
	var creatorSearchOption = document.getElementById("map-search-creator-name-input").value;
	
	var searchRequest = ' { "message_id" : "search_maps", "message_type" : "lobby", "lobby_id" : ' + lobbyId + ', "size_criteria" : "' + sizeSearchOption;
	if (creatorSearchOption.length != 0){
		searchRequest += '", "creator_criteria" : "' + creatorSearchOption;
	}
	
	searchRequest += '" }';
	console.log("sending search request: " + searchRequest.lobby_id);
	socket.send(searchRequest);
}

function refreshLobbyList(lobbyMessage){
	console.log(lobbyMessage);
	lobbyId = lobbyMessage.lobby_id;
	var lobbyMembers = lobbyMessage.lobby_members;
	document.getElementById("left-user-lobby-label").innerHTML = lobbyMembers[0].name;
	if (lobbyMembers.length > 1){
		document.getElementById("right-user-lobby-label").innerHTML = lobbyMembers[1].name;
	}else{
		rightVotedMap = null;
		isRightVoteRandom = false;
		document.getElementById("right-user-lobby-label").innerHTML = "n/a";
	}
	
	var table = document.getElementById("lobby-member-list");
	var lobbyHeader = document.getElementById("lobby-header");
	isLeftSidePlayer = lobbyMessage.is_left_side;

	lobbyHeader.innerHTML = "Lobby - ID: " + lobbyId;
	document.getElementById("lobby-players-connected").innerHTML = " Players Connected: " + lobbyMembers.length;
	
	drawMapVotes();
}

function refreshMapOptionsTable(mapList){
	var mapResults = document.getElementById("map-list-results");
	var mapResultsText = document.getElementById("map-list-results-text");
	mapResults.innerHTML = "";
	mapResultsText.innerHTML = "";
	if ((isLeftSidePlayer && leftVotedMap != null) || (!isLeftSidePlayer && rightVotedMap != null)){
		mapResultsText.innerHTML = "Map selected, waiting for other user..";
	}else{
		mapResultsText.innerHTML = "Choose a map (" + mapList.length + ")";
	}
	
	for(let i = 0; i < mapList.length; i++){
		let mapOptionCanvas = document.createElement('canvas');
		mapOptionCanvas.style.padding = "10px 10px 10px 10px";
		
		mapResults.appendChild(mapOptionCanvas);
		drawMinimap(mapOptionCanvas, mapList[i]);
		
		mapOptionCanvas.onclick = function(){
			console.log("selected map ID: " + mapList[i].id);
			socket.send(' { "message_id" : "selected_map", "message_type" : "lobby", "lobby_id" : ' + lobbyId + ', "map_id" : ' + mapList[i].id + ' }');
		}
	}
}

function updateLobbyHeader(string){
	document.getElementById("lobby-header").innerHTML = string;
}

function voteForRandomMap(){
	var randomMapSearch = ' { "message_id" : "random_map_select", "message_type" : "lobby", "lobby_id" : ' + lobbyId + '}';
	socket.send(randomMapSearch);
}
