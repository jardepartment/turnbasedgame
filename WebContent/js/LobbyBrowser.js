// LobbyBrowser.js 
//    - Contains functions/variables associated with "lobby-browser-state" 

function attemptToJoinLobby(lobbyId){
	var joinLobbyMessage = '{ "message_type" : "lobby", "message_id" : "join_lobby", "lobby_id" : ' + lobbyId + '}';
	socket.send(joinLobbyMessage);
}

function displayLobbies(lobbies){
	console.log(lobbies);
	document.getElementById("lobbies-found-label").innerHTML = "Lobbies found: " + lobbies.length;
	var lobbyTable = document.getElementById("lobby-list-table");
	lobbyTable.innerHTML = "";
	// using let to save scope of i, else every button would join the last
	// lobby added from the array
	for(let i = 0; i < lobbies.length; i++){
		let joinButton = document.createElement('button');
		joinButton.innerHTML = "Join <b>" + lobbies[i].left_lobby_member.name + "</b>'s Lobby -  #" + lobbies[i].lobby_id;
		joinButton.onclick = function(){
			console.log(lobbies[i].lobby_id);
			attemptToJoinLobby(lobbies[i].lobby_id);
		}

		joinButton.classList.add("main-menu-button");

		var row = document.createElement('tr');
		var joinButtonTd = document.createElement('td');
		joinButtonTd.appendChild(joinButton);
		row.appendChild(joinButtonTd);
		
		lobbyTable.appendChild(row);
	}
}

function lobbyBrowserMessageHandler(message){
	var lobbyMessage = JSON.parse(message.data);
	switch(lobbyMessage.message_id){
		case "lobby_list_update":
			displayLobbies(lobbyMessage.lobbies);
			break;
		case "join_lobby_response":
			if(lobbyMessage.is_successful){
				changeStates("lobby-state");
			}else{
				alert("lobby no longer exists");
				browseLobbies();
			}
			break;
		case "lobby_member_update":
			changeStates("lobby-state");
			refreshLobbyList(lobbyMessage);
			break;
		case "player_count_broadcast":
			// ignore
			break;
		default:
			console.log("unknown message received in lobbyBrowserMessageHandler:" + lobbyMessage.message_id);
	}
}
