// Battle.js 
//    - Contains functions/variables associated with "in-battle-state", drives the
//      frame counter and animation within the game.

battleId = 0;
ownUnits = [];
enemyUnits = [];
hoveredCoordinate = {};
animationFrame = 0;
FPS_DELAY = 40;//1000/40 = 25fps

var focusX = 0;
var focusY = 0;
var possibleAttacks = [];
var possibleMoves = [];
var attackableTiles = [];
var healableTiles = [];
var possibleHeals = [];
var mapChanged = true;
var idleAnimationFrameCounter = 0;

var overviewCanvas = document.getElementById("overview-ctx");
var overviewCtx = overviewCanvas.getContext("2d");

var chatForm = document.getElementById("chat-form");
var chatInput = document.getElementById("chat-input");
var chatText = document.getElementById("chat-text");

setInterval(function(){
	if(clientState != "in-battle-state"){
		return;
	}
	
	if (idleAnimationFrameCounter >= 10){
		incrementAnimationFrame();
		idleAnimationFrameCounter = 0;
	}
	
	idleAnimationFrameCounter++;
	
	if(mapChanged){
		redrawMapCanvas(focusX, focusY);
		mapChanged = false;
	}
	
	redrawUnitCanvas(focusX, focusY);
	redrawUiCanvas(focusX, focusY);
}, FPS_DELAY);

chatForm.onsubmit = function(e){
	e.preventDefault();
	if(chatInput.value === ""){
		return;
	}
	
	socket.send('{"message_type":"battle", "battle_id" : ' + battleId + ', "message_id": "new_chat_message", "chat_contents":"' + chatInput.value+'"}');
	chatInput.value = '';
}

function battleMessageHandler(message){
	var battleMessage = JSON.parse(message.data);
	var messageId = battleMessage.message_id;
	switch(messageId){
		case "battle_map_init":
			handleBattleMapInit(battleMessage);
			break;
		case "battle_overview":
			updateBattleOverview(battleMessage);
			break;
		case "start_turn":
			handleTurnStart(battleMessage);
			break;
		case "end_turn":
			handleEndTurn();
			break;
		case "unit_move":
			handleUnitMove(battleMessage);
			break;
		case "update_units":
			handleUpdateUnits(battleMessage);
			break;
		case "user_selection_response":
			handleSelection(battleMessage.tile_info, battleMessage.unit_info);
			break;
		case "movement_error":
			handleMovementError();
			break;
		case "attack_results":
			handleAttackResults(battleMessage);
			break;
		case "heal_results":
			handleHealResults(battleMessage);
			break;
		case "battle_over":
			endBattle(battleMessage);
			break;
		case "chat_message":
			handleChatMessage(battleMessage);
			break;
		case "lobby_member_update":
			//no action needed, other player disconnected
			break;
		default:
			console.log("battleMessageHandler received unknown message");
			console.log(battleMessage);
	}
}

function changeGraphics(){
	console.log("here");
	if(SIMPLE_2D_GRAPHICS){
		SIMPLE_2D_GRAPHICS = false;
	}else{
		SIMPLE_2D_GRAPHICS = true;
	}
	
	refreshGraphicalUnitCoords();
	mapChanged = true;
}

function convertMultiplierToPercentageString(multiplier){
	if(multiplier == undefined){
		return "+0%";
	}
	
	if(multiplier >= 0.0){
		return ("+"+(multiplier*100).toFixed()+"%");
	}else{
		// negative number already has "-" prepended
		return ((multiplier*100).toFixed()+"%");
	}
}

function deselectUnit(){
	
	document.getElementById("selected-unit-modal-title").innerHTML = "- no unit selected -";
	
	var unitInfoCanvas = document.getElementById("unit-info-canvas");
	var unitInfoCtx = unitInfoCanvas.getContext("2d");
	
	unitInfoCtx.clearRect(0, 0, unitInfoCanvas.width, unitInfoCanvas.height);
	
	$("#selected-unit-description").empty();
	$("#basic-combat-info-table").hide();
	$("#basic-movement-info-table").hide();
	$("#advanced-movement-info-table").hide();
	$("#advance-attacking-info-list").hide();
	$("#advanced-defending-info-list").hide();

}

function endBattle(message){
	console.log(message);
	var displayStr;
	if(message.winner){
		displayStr = "Victory! Congrats, you beat " + message.losing_player_name +"!";
	}else{
		displayStr = "Defeat! You lost to " + message.winning_player_name;
	}
	
	document.getElementById("battle-state-label").innerHTML = "GAME OVER";
	document.getElementById("game-over-modal-title").innerHTML = displayStr;
	$("#game-over-modal").modal();
	socket.send('{"message_type":"battle", "battle_id" : ' + battleId + ', "message_id": "game_over"}');
}

function getAttackableTiles(){
	return attackableTiles;
}

function getHealableTiles(){
	return healableTiles;
}

function getPossibleMoves(){
	return possibleMoves;
}

function handleAttackResults(message){
	var results = message.results;
	if (!results.success){
		console.log("unable to attack!");
		return;
	}
	
	var damageDealt = results.damage_dealt;
	var attackerX = results.attacker.x;
	var attackerY = results.attacker.y;
	var defenderX = results.defender.x;
	var defenderY = results.defender.y;
	var isCritical = results.is_critical_hit;
	var unitKilled = results.unit_killed;
	drawAttackDamage(damageDealt, focusX, focusY, attackerX, attackerY, defenderX, defenderY, isCritical);
	logAttack(message.attacker_username, results.attacker, message.attacked_username, results.defender, damageDealt, isCritical, unitKilled);
}

function handleBattleMapInit(battleMessage){
	battleId = battleMessage.battle_id;
	chatText.innerHTML = '';
	initializeGraphics(battleMessage, "map-ctx", "unit-ctx", "ui-ctx", "information-ctx", true);
	mapChanged = true;
}

function handleChatMessage(message){
	var currentTime = new Date().toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});
	chatText.innerHTML += '<div style=\"font-size:xx-small; float: right;\">'
		+ currentTime +'</div><div style=\"text-align: left;\"><b>'
			+ message.player_name +'</b>: '
			+ message.chat_contents +'</div>';
	$('#chat-text').scrollTop($('#chat-text')[0].scrollHeight);
}

function handleEndTurn(){
	document.getElementById("battle-state-label").innerHTML = "waiting...";
	document.getElementById("end-turn-button").style.display = "none";
	document.getElementById("battle-remaining-actions-table").style.display = "none";
	isPlayerTurn = false;
}

function handleMovementError(){
	if (inSetupState){
		alert("unable to spawn unit here");
	}else{
		alert("invalid move requested");
	}
}

function handleHealResults(message){
	var results = message.results;
	if (!results.success){
		console.log("unable to heal!");
		return;
	}
	
	var healingDealt = results.healing_dealt;
	var healerX = results.healer.x;
	var healerY = results.healer.y;
	var healedUnitX = results.unit_to_heal.x;
	var healedUnitY = results.unit_to_heal.y;
	drawHealthRegained(healingDealt, focusX, focusY, healerX, healerY, healedUnitX, healedUnitY);
	logHeal(message.healer_username, results.healer, results.unit_to_heal, healingDealt);
}

function handleSelection(tileInfo, unitInfo){
	possibleMoves = [];
	attackableTiles = [];
	healableTiles = [];
	possibleHeals = [];
	
	if(tileInfo != undefined){
		selectTile(tileInfo);
	}
	
	if(unitInfo != undefined){
		selectUnit(unitInfo.unit);
		possibleMoves = unitInfo.possible_moves;
		attackableTiles = unitInfo.attackable_tiles;
		possibleAttacks = unitInfo.possible_attacks;
		healableTiles = unitInfo.healable_tiles;
		possibleHeals = unitInfo.possible_heals;
	}else{
		deselectUnit();
	}
}

function handleTurnStart(message){
	document.getElementById("battle-state-label").innerHTML = "It is your turn!";
	document.getElementById("end-turn-button").style.display = "block";
	isPlayerTurn = true;
	document.getElementById("knock_audio").play();
}

function handleUnitMove(moveMessage){
	
	var unitId = moveMessage.unit.id;
	var finalLocation = moveMessage.move.final_location;
	var steps = moveMessage.move.steps;
	
	var unit = undefined;
	for(var i in ownUnits){
		if (ownUnits[i].id == unitId){
			unit = ownUnits[i];
			break;
		}
	}
	
	if (unit == undefined){
		for(var i in enemyUnits){
			if (enemyUnits[i].id == unitId){
				unit = enemyUnits[i];
				break;
			}
		}
	}
	
	var FRAMES_PER_STEP = 10;
	var stepIndex = 0;
	var nextStep = steps[stepIndex];
	var stepDelta = getStepDelta(unit.x, unit.y, nextStep.x, nextStep.y);
	var dy = Math.sin(stepDelta.angle_in_radians) * (stepDelta.distance_between / FRAMES_PER_STEP);
	var dx = Math.cos(stepDelta.angle_in_radians) * (stepDelta.distance_between / FRAMES_PER_STEP);
	var currentFrame = 0;
	
	if (dy > 0){ //since the map and units are displayed row by row, need to pre-increase their game tile Y value when moving up
		unit.x = nextStep.x;
		unit.y = nextStep.y;
	}
	
	const moveInterval = setInterval(function() {
		unit.graphical_coord.y += dy;
		unit.graphical_coord.x += dx;
		
		if (currentFrame >= FRAMES_PER_STEP){
			currentFrame = 0;
			unit.x = nextStep.x;
			unit.y = nextStep.y;
			if ((unit.x == finalLocation.x) && (unit.y == finalLocation.y)){
				refreshGraphicalUnitCoords();
				clearInterval(moveInterval);
			}else{
				refreshGraphicalUnitCoords();
				stepIndex++;
				nextStep = steps[stepIndex];
				stepDelta = getStepDelta(unit.x, unit.y, nextStep.x, nextStep.y);
				dy = Math.sin(stepDelta.angle_in_radians) * (stepDelta.distance_between / FRAMES_PER_STEP);
				dx = Math.cos(stepDelta.angle_in_radians) * (stepDelta.distance_between / FRAMES_PER_STEP);
				
				if (dy > 0){
					unit.x = nextStep.x;
					unit.y = nextStep.y;
				}
			}
		}

		currentFrame++;
	}, FPS_DELAY);
}

function handleUpdateUnits(battleMessage){
	ownUnits = battleMessage.own_units;
	if(!inSetupState){
		refreshOwnUnits(ownUnits, (battleMessage.number_of_moves_left != 0), (battleMessage.number_of_actions_left != 0));
		refreshActionsRemainingDiv(battleMessage.number_of_actions_left, battleMessage.number_of_moves_left);
	}else{
		refreshOwnUnits(ownUnits, true, false);
	}
	
	if(battleMessage.enemy_units != undefined){
		console.log(enemyUnits);
		enemyUnits = battleMessage.enemy_units;
		refreshEnemyUnitList(enemyUnits);
	}
	
	refreshGraphicalUnitCoords();
}

function incrementAnimationFrame(){
	//TODO
	animationFrame++;
	if (animationFrame >= 6){
		animationFrame = 0;
	}
}

function logAttack(attackerUsername, attacker, defenderUsername, defender, damageDealt, isCritical, unitKilled){
	var currentTime = new Date().toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});
	var damageString = '<div class=\"glyphicon glyphicon-screenshot\"></div><b> ' + damageDealt + ' DMG';
	if (isCritical){
		damageString += ' (CRIT!)';
	}
	
	if(unitKilled){
		damageString += '</b> - ' + attackerUsername + '\'s ' + attacker.name + ' killed ' + defenderUsername + '\'s ' + defender.name;
	}else{
		damageString += '</b> - ' + attackerUsername + '\'s ' + attacker.name + ' hit ' + defenderUsername + '\'s ' + defender.name;
	}
	
	var styleString = '';
	if(unitKilled){
		styleString='\"text-align:left; color:darkred; font-size:small;\"';
	}else{
		styleString='\"text-align:left; color:darkgrey; font-size:small;\"';
	}
	
	chatText.innerHTML += '<div style=\"font-size:xx-small; float: right;\">'
		+ currentTime +'</div><div style='+ styleString + '><i>'+ damageString +'</i></div>';
	$('#chat-text').scrollTop($('#chat-text')[0].scrollHeight);
}

function logHeal(healerUsername, healer, unitToHeal, healingDealt){
	var currentTime = new Date().toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});
	var healingString = '<div class=\"glyphicon glyphicon-plus-sign\"></div><b> ' + healingDealt + ' HP';
	healingString += '</b> - ' + healerUsername + '\'s ' + healer.name + ' healed ' + unitToHeal.name;
	
	var styleString ='\"text-align:left; color:darkgrey; font-size:small;\"';
	
	chatText.innerHTML += '<div style=\"font-size:xx-small; float: right;\">'
		+ currentTime +'</div><div style='+ styleString + '><i>'+ healingString +'</i></div>';
	$('#chat-text').scrollTop($('#chat-text')[0].scrollHeight);
}

function refreshActionsRemainingDiv(actionsLeft, movesLeft){
	if(!isPlayerTurn){
		return;
	}
	
	var actionsLeftStr = "Actions remaining: " + actionsLeft;
	var movesLeftStr = "Moves remaining: " + movesLeft;
	document.getElementById("battle-remaining-actions-table").style.display = "block";
	document.getElementById("actions-remaining-row").innerHTML = actionsLeftStr;
	document.getElementById("moves-remaining-row").innerHTML = movesLeftStr;
}

function sendPlayerReady(){
	var playerReadyMessage = '{ "message_type" : "battle", "battle_id" : ' + battleId + ', "message_id" : "player_ready" }';
	socket.send(playerReadyMessage);
	document.getElementById("player-ready-button").style.display = "none";
	document.getElementById("battle-state-label").innerHTML = "waiting for other player(s) to ready up...";
	document.getElementById("battle-state-label").style.display = "block";
	inSetupState = false;
	redrawMapCanvas(focusX, focusY);
}

function tileContainsUnit(ownUnit, x, y){
	var units = [];
	if(ownUnit){
		units = ownUnits;
	}else{
		units = enemyUnits;
	}
	
	for (var i in units){
		if (units[i].is_alive && (units[i].x == x) && (units[i].y == y)){
			return true;
		}
	}
	
	return false;
}

function updateBattleOverview(battleMessage){

	var leftUser = battleMessage.left_user.name;
	var rightUser = battleMessage.right_user.name;
	var leftUnitHealth = battleMessage.chances.left_unit_health;
	var leftUnitStrength = battleMessage.chances.left_unit_strength;
	var rightUnitHealth = battleMessage.chances.right_unit_health;
	var rightUnitStrength = battleMessage.chances.right_unit_strength;
	var leftPercentage = battleMessage.chances.left_percentage;
	
	var visualPercentageDiff = (.5-leftPercentage)*3;
	
	var visualPercentage = .5 - visualPercentageDiff; 
	
	overviewCtx.lineWidth = 1;
	overviewCtx.clearRect(0, 0, overviewCanvas.width, overviewCanvas.height);
	if(isLeftSidePlayer){
		overviewCtx.fillStyle = "rgba(173, 207, 161)";
	}else{
		overviewCtx.fillStyle = "rgba(217, 207, 161)";
	}
	
	overviewCtx.fillRect(0,0,overviewCanvas.width*visualPercentage, overviewCanvas.height);
	
	if(!isLeftSidePlayer){
		overviewCtx.fillStyle = "rgba(173, 207, 161)";
	}else{
		overviewCtx.fillStyle = "rgba(217, 207, 161)";
	}
	
	overviewCtx.fillRect(overviewCanvas.width*visualPercentage,0,overviewCanvas.width*(1-visualPercentage), overviewCanvas.height);
	
	
	document.getElementById("left-user-label").innerHTML = '<i>'+leftUser+'</i>';
	document.getElementById("right-user-label").innerHTML = '<i>'+rightUser+'</i>';
}

function updateInfoTable(unit){
	
	//TODO hardcoded healer traits
	var healerRange = 2.5;
	var healerAmount = 25;
	
	$("#basic-combat-info-table").show();
	$("#basic-movement-info-table").show();
	$("#advanced-movement-info-table").show();
	$("#advance-attacking-info-list").show();
	$("#advanced-defending-info-list").show();
	
	document.getElementById("selected-unit-modal-title").innerHTML = unit.name + " | <i>"+unit.unit_class+"</i>";
	
	var unitInfoCanvas = document.getElementById("unit-info-canvas");
	var unitInfoCtx = unitInfoCanvas.getContext("2d");
	
	
	var unitImage = document.getElementById(unit.unit_faction+"-"+unit.unit_class+"-sprite");
	
	unitInfoCtx.clearRect(0, 0, unitInfoCanvas.width, unitInfoCanvas.height);
	unitInfoCtx.drawImage(unitImage, // source image
            0,      // start of x on source
            0,      // start of y on source
            108,    // width to take from source
            108,    // height to take from source
            0,      // start x on canvas to draw
            0,      // start y on canvas to draw
            216,    // width on canvas
            108);   // height on canvas
	
	$("#selected-unit-description").empty();
	$("#selected-unit-description").append("<div>"+unit.description+"</div>");
	
	$("#basic-combat-info-table").empty();
	$("#basic-combat-info-table").prepend("<caption style=\"text-align:center;\">Combat</caption>");
	$("#basic-combat-info-table").append("<tr title=\"The current amount of health this unit has. Units will die at 0 or less health.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus-sign\"></div> Health</td><td style=\"float:right; padding-right:15px;\">"+unit.current_health+"/" + unit.maximum_health+"</td></tr>");
	
	if(unit.combat_traits.base_attack_strength != undefined){
		$("#basic-combat-info-table").append("<tr title=\"The base attack damage of this unit that multipliers will get applied to.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-screenshot\"></div> Damage</td><td style=\"float:right; padding-right:15px;\">"+unit.combat_traits.base_attack_strength+"</td></tr>");
		$("#basic-combat-info-table").append("<tr title=\"Attack range in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-screenshot\"></div> Range</td><td style=\"float:right; padding-right:15px;\">"+unit.combat_traits.attack_range+"</td></tr>");
	}else{
		$("#basic-combat-info-table").append("<tr title=\"The amount of health a healed unit will regain.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus\"></div> Healing</td><td style=\"float:right; padding-right:15px;\">"+healerAmount+"</td></tr>");
		$("#basic-combat-info-table").append("<tr title=\"Healing range in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus\"></div> Range</td><td style=\"float:right; padding-right:15px;\">"+healerRange+"</td></tr>");
	}

	$("#basic-movement-info-table").empty();
	$("#basic-movement-info-table").prepend("<caption style=\"text-align:center;\">Movement</caption>");
	$("#basic-movement-info-table").append("<tr title=\"Maximum distance this unit can move in a single turn in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\" style=\"font-size:x-large;\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-share-alt\"></div> Range:</td><td style=\"float:right; padding-right:15px;\">"+unit.movement_traits.max_movement_range+"</td></tr>");
	
	
	$("#advanced-movement-info-table").empty();
	$("#advanced-movement-info-table").prepend("<caption style=\"text-align:center;\">Movement</caption>");
	$("#advanced-movement-info-table").append("<tr title=\"Maximum distance this unit can move in a single turn in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-share-alt\"></div> Movement Range</td><td style=\"float:right; padding-right:15px;\">"+unit.movement_traits.max_movement_range+"</td></tr>");
	$("#advanced-movement-info-table").append("<tr title=\"The biggest difference in elevation this can unit can move to from the current tile (inclusive).\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-sort\"></div> Max. elev. change</td><td style=\"float:right; padding-right:15px;\">"+unit.movement_traits.max_elevation_delta+"</td></tr>");
	
	$("#advanced-attacking-info-table").empty();
	$("#advanced-attacking-info-table-unitclass").empty();
	$("#advanced-attacking-info-table-elevation").empty();
	
	if(unit.combat_traits.base_attack_strength != undefined){
		var criticalHitProbability = (unit.combat_traits.critical_hit_probability * 100).toFixed() + "%";
		var criticalHitMultiplier = convertMultiplierToPercentageString(unit.combat_traits.critical_hit_multiplier);

		var attackingMelee = convertMultiplierToPercentageString(unit.combat_traits.attacking_against_unit_class_multipliers.melee);
		var attackingRanged = convertMultiplierToPercentageString(unit.combat_traits.attacking_against_unit_class_multipliers.ranged);
		var attackingMounted = convertMultiplierToPercentageString(unit.combat_traits.attacking_against_unit_class_multipliers.mounted);
		
		var attackingUphill = convertMultiplierToPercentageString(unit.combat_traits.attacking_with_elevation_delta_multipliers.uphill);
		var attackingLevel = convertMultiplierToPercentageString(unit.combat_traits.attacking_with_elevation_delta_multipliers.level);
		var attackingDownhill = convertMultiplierToPercentageString(unit.combat_traits.attacking_with_elevation_delta_multipliers.downhill);
		
		$("#advanced-attacking-info-table").prepend("<caption title=\"Primary action of this unit is to attack enemy units.\" style=\"text-align:center;\">Attacking</caption>");
		$("#advanced-attacking-info-table").append("<tr title=\"The base attack damage of this unit that multipliers will get applied to.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-screenshot\"></div> Base Damage</td><td style=\"float:right; padding-right:15px;\">"+unit.combat_traits.base_attack_strength+"</td></tr>");
		$("#advanced-attacking-info-table").append("<tr title=\"Attack range in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-screenshot\"></div> Attack Range</td><td style=\"float:right; padding-right:15px;\">"+unit.combat_traits.attack_range+"</td></tr>");
		$("#advanced-attacking-info-table").append("<tr title=\"Every attack has this chance to apply a critical hit multipler.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-flash\"></div> Critical Hit Chance</td><td style=\"float:right; padding-right:15px;\">"+criticalHitProbability+"</td></tr>");
		$("#advanced-attacking-info-table").append("<tr title=\"Multiplier only added on a critical hit.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-chevron-up\"></div> Critical Hit Multiplier</td><td style=\"float:right; padding-right:15px;\">"+criticalHitMultiplier+"</td></tr>");
		$("#advanced-attacking-info-table-unitclass").append("<tr title=\"Modifiers when attacking units of a certain class (positive/large values are better).\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-user\"></div> Unit Class Multipliers</td></tr>");
		$("#advanced-attacking-info-table-unitclass").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-pawn\"></div> vs Melee</td><td style=\"float:right; padding-right:15px;\">"+attackingMelee+"</td></tr>");
		$("#advanced-attacking-info-table-unitclass").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-record\"></div> vs Ranged</td><td style=\"float:right; padding-right:15px;\">"+attackingRanged+"</td></tr>");
		$("#advanced-attacking-info-table-unitclass").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-knight\"></div> vs Mounted</td><td style=\"float:right; padding-right:15px;\">"+attackingMounted+"</td></tr>");
		$("#advanced-attacking-info-table-elevation").append("<tr title=\"The biggest difference in elevation this can unit can attack over (inclusive).\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-sort\"></div> Max. elev. change</td><td style=\"float:right; padding-right:15px;\">"+unit.combat_traits.max_elevation_delta+"</td></tr>");
		$("#advanced-attacking-info-table-elevation").append("<tr title=\"Modifiers applied when attacking uphill, downhill or level.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-stats\"></div> Elevation Multipliers</td></tr>");
		$("#advanced-attacking-info-table-elevation").append("<tr title=\"Attacking a unit which is at a higher elevation than this unit.\"><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-triangle-top\"></div> Uphill</td><td style=\"float:right; padding-right:15px;\">"+attackingUphill+"</td></tr>");
		$("#advanced-attacking-info-table-elevation").append("<tr title=\"Attacking a unit which is at the same elevation as this unit.\"><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-minus\"></div> Level</td><td style=\"float:right; padding-right:15px;\">"+attackingLevel+"</td></tr>");
		$("#advanced-attacking-info-table-elevation").append("<tr title=\"Attacking a unit which is at a lower elevation than this unit.\"><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-triangle-bottom\"></div> Downhill</td><td style=\"float:right; padding-right:15px;\">"+attackingDownhill+"</td></tr>");
	}else{
		$("#advanced-attacking-info-table").prepend("<caption title=\"Primary action of this unit is to heal other units.\" style=\"text-align:center;\">Healing</caption>");
		$("#advanced-attacking-info-table").append("<tr title=\"The amount of health a healed unit will regain.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus\"></div> Healing amount</td><td style=\"float:right; padding-right:15px;\">"+healerAmount+"</td></tr>");
		$("#advanced-attacking-info-table").append("<tr title=\"Healing range in tile widths. Corner-adjacent tiles are ~1.4 tile widths apart.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus\"></div> Range</td><td style=\"float:right; padding-right:15px;\">"+healerRange+"</td></tr>");
	}
	var treesDefense = convertMultiplierToPercentageString(unit.combat_traits.defending_on_tile_multipliers.trees);
	var waterDefense = convertMultiplierToPercentageString(unit.combat_traits.defending_on_tile_multipliers.water);
	var grassDefense = convertMultiplierToPercentageString(unit.combat_traits.defending_on_tile_multipliers.grass);
	var rockDefense = convertMultiplierToPercentageString(unit.combat_traits.defending_on_tile_multipliers.rock);
	var sandDefense = convertMultiplierToPercentageString(unit.combat_traits.defending_on_tile_multipliers.sand);
	
	$("#advanced-defending-info-table").empty();
	$("#advanced-defending-info-table").prepend("<caption style=\"text-align:center;\">Defending</caption>");
	$("#advanced-defending-info-table").append("<tr title=\"The current amount of health this unit has. Units will die at 0 or less health.\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-plus-sign\"></div> Health</td><td style=\"float:right; padding-right:15px;\">"+unit.current_health+"/" + unit.maximum_health+"</td></tr>");
	$("#advanced-defending-info-table-tiles").empty();
	$("#advanced-defending-info-table-tiles").append("<tr title=\"Applied to the attacker's damage output when this unit is defending from a tile of the following types (positive/large values are better).\"><td style=\"padding-left:15px;\"><div class=\"glyphicon glyphicon-tower\"></div> Tile Type Multipliers</td></tr>");
	$("#advanced-defending-info-table-tiles").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-menu-right\"></div> Trees</td><td style=\"float:right; padding-right:15px;\">"+treesDefense+"</td></tr>");
	$("#advanced-defending-info-table-tiles").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-menu-right\"></div> Water</td><td style=\"float:right; padding-right:15px;\">"+waterDefense+"</td></tr>");
	$("#advanced-defending-info-table-tiles").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-menu-right\"></div> Grass</td><td style=\"float:right; padding-right:15px;\">"+grassDefense+"</td></tr>");
	$("#advanced-defending-info-table-tiles").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-menu-right\"></div> Rock</td><td style=\"float:right; padding-right:15px;\">"+rockDefense+"</td></tr>");
	$("#advanced-defending-info-table-tiles").append("<tr><td style=\"padding-left:30px;\"><div class=\"glyphicon glyphicon-menu-right\"></div> Sand</td><td style=\"float:right; padding-right:15px;\">"+sandDefense+"</td></tr>");
}

//******************************************************************************************
// TODO below should move to a Ui.js file and copied code in MapCreate.js should be removed.
//******************************************************************************************

var clickAndDragDeadZone = 5;
var clickingMouse1 = false;
var movingMap = false;
var lastClickedX = 0;
var lastClickedY = 0;
var lastOffsetX = 0;
var lastOffsetY = 0;
var lastHoveredCoordinate = {};
hoveredOnAttack = false;
hoveredOnHeal = false;

var $uiCtx = $('#ui-ctx');
$uiCtx.on('mousemove', function handler(event) {
    if (clickingMouse1 && ((Math.abs(event.offsetX-lastOffsetX) > clickAndDragDeadZone) || 
    					   (Math.abs(event.offsetY-lastOffsetY) > clickAndDragDeadZone) || 
    					    movingMap)) {
    	focusX = lastClickedX-event.offsetX;
    	focusY = lastClickedY-event.offsetY;
    	movingMap = true;
    	mapChanged = true;
    }
    
    hoveredCoordinate = getSelectedCoordinate(event.offsetX, event.offsetY, focusX, focusY);
    if (hoveredCoordinate == null){
    	
    } else if((hoveredCoordinate.x != lastHoveredCoordinate.x) || (hoveredCoordinate.y != lastHoveredCoordinate.y)){
    	lastHoveredCoordinate = hoveredCoordinate;
    	clearSimulatedAttackResults();
    	hoveredOnAttack = false;
    	hoveredOnHeal = false;
    	if (tileContainsUnit(false, hoveredCoordinate.x, hoveredCoordinate.y)){
    		for (var i in possibleAttacks){
    			if ((possibleAttacks[i].enemy_x == hoveredCoordinate.x) && (possibleAttacks[i].enemy_y == hoveredCoordinate.y)){
    				drawSimulatedAttackResults(possibleAttacks[i], focusX, focusY);
    				hoveredOnAttack = true;
    				break;
    			}
    		}
    	}
    	
    	if (tileContainsUnit(true, hoveredCoordinate.x, hoveredCoordinate.y)){
	    	for (var i in possibleHeals){
				if ((possibleHeals[i].friendly_x == hoveredCoordinate.x) && (possibleHeals[i].friendly_y == hoveredCoordinate.y)){
					drawSimulatedHealthResults(possibleHeals[i], focusX, focusY);
					hoveredOnHeal = true;
					break;
				}
			}
    	}
    }
});

$uiCtx.on('dblclick', function handler(event){
	//TODO
});

$uiCtx.on('mousedown', function(event){
	lastClickedX = event.offsetX+focusX;
	lastClickedY = event.offsetY+focusY;
	lastOffsetX = event.offsetX;
	lastOffsetY = event.offsetY;
	clickingMouse1 = true;
});

$uiCtx.on('mouseup', function(event){
	if(!movingMap){
		var selectedCoords = getSelectedCoordinate(event.offsetX, event.offsetY, focusX, focusY);
		performTileSelect(selectedCoords.x, selectedCoords.y);
	}
	
	clickingMouse1 = false;
	movingMap = false;
	
});

$uiCtx.on('mouseleave', function(event){
	clickingMouse1 = false;
	movingMap = false;
});

$uiCtx.on('wheel',function(event){
	if (event.originalEvent.deltaY < 0) {
		mapZoom(true);
	}
	
	if (event.originalEvent.deltaY > 0) {
		mapZoom(false);
	}
	
	mapChanged = true;
	refreshGraphicalUnitCoords();
	
    event.preventDefault();
});

$uiCtx.on('contextmenu', function(event){
	var selectedCoords = getSelectedCoordinate(event.offsetX, event.offsetY, focusX, focusY);
	
	if (tileContainsUnit(true, selectedCoords.x, selectedCoords.y)){
		performQuickAction(selectedCoords.x, selectedCoords.y, "attack");
	} else if (tileContainsUnit(false, selectedCoords.x, selectedCoords.y)){
		performQuickAction(selectedCoords.x, selectedCoords.y, "attack");
	} else {
		performQuickAction(selectedCoords.x, selectedCoords.y, "unit_move");
	}
	
	setSelectedTile(selectedCoords.x, selectedCoords.y);
});
