// Graphics.js 
//    - Contains functions used to render graphics within the game. 
//      You are most likely not going to enjoy looking at this file..

var CANVAS_WIDTH = 800;
var CANVAS_HEIGHT = 600;

var MIN_TILE_IMAGE_SIZE = 25;
var MAX_TILE_IMAGE_SIZE = 144;
var TILE_IMAGE_SIZE = 64;
SIMPLE_2D_GRAPHICS = false;

//used for 2d map display
var GRASS_COLORS = ["#2d4a22", "#3c632d", "#4c7c39", "#5b9544", "#6aad50", "#79c65b"];
var SAND_COLORS  = ["#605d3e", "#807c53", "#9f9b67", "#bfba7c", "#dfd990", "#fff8a5"];
var ROCK_COLORS =  ["#4d4d4d", "#676767", "#808080", "#9a9a9a", "#b3b3b3", "#cdcdcd"];
var WATER_COLORS = ["#27395b", "#354d7a", "#426099", "#4f73b7", "#5c86d6", "#6999f4"];


//***** 3d style graphics **************
//
//   SKEW      WIDTH
//   __+_   _____+____
//  /    \ /          \
// |      |            |
//  ___________________    ___
// |      _____________|   ___}  PADDING
// |     /            /|      \
// |    /            / |       \
// |   /            /  |        } HEIGHT
// |  /            /   |       /
// | /            /   /|      /
// |/____________/   / |   __/
// |             |  /  |     \ 
// |             | /   |      }   RISE
// |_____________|/____|   __/

var MAX_ELEVATION_POSSIBLE = 5;

var BASE_IMAGE_DIMENSIONS = 144;
var BASE_IMAGE_WIDTH = 112;
var BASE_IMAGE_HEIGHT = 100;
var BASE_IMAGE_RISE = 36;

var WIDTH = TILE_IMAGE_SIZE*(BASE_IMAGE_WIDTH/BASE_IMAGE_DIMENSIONS);
var HEIGHT = TILE_IMAGE_SIZE*(BASE_IMAGE_HEIGHT/BASE_IMAGE_DIMENSIONS);
var RISE = TILE_IMAGE_SIZE*(BASE_IMAGE_RISE/BASE_IMAGE_DIMENSIONS);
var SKEW = TILE_IMAGE_SIZE-WIDTH;
var PADDING = TILE_IMAGE_SIZE-HEIGHT-RISE;

var BASE_UNIT_IMAGE_DIMENSIONS = 108;
var UNIT_IMAGE_SIZE = TILE_IMAGE_SIZE*(BASE_UNIT_IMAGE_DIMENSIONS/BASE_IMAGE_DIMENSIONS);

var reset3dMapDimensions = function(){
	WIDTH = TILE_IMAGE_SIZE*(BASE_IMAGE_WIDTH/BASE_IMAGE_DIMENSIONS);
	HEIGHT = TILE_IMAGE_SIZE*(BASE_IMAGE_HEIGHT/BASE_IMAGE_DIMENSIONS);
	RISE = TILE_IMAGE_SIZE*(BASE_IMAGE_RISE/BASE_IMAGE_DIMENSIONS);
	SKEW = TILE_IMAGE_SIZE-WIDTH;
	PADDING = TILE_IMAGE_SIZE-HEIGHT-RISE;
	
	// reset unit image size
	UNIT_IMAGE_SIZE = TILE_IMAGE_SIZE*(BASE_UNIT_IMAGE_DIMENSIONS/BASE_IMAGE_DIMENSIONS);
}
//**************************************

var mapCanvas;
var mapCtx;

var unitCanvas;
var unitCtx;

var uiCanvas;
var uiCtx;

var informationCanvas;
var informationCtx;

var mapJson;
var centerOfTileFaces = [];

inSetupState = false;

var addOwnUnitsList = function(){
	$("#own-units-list").style.display = "block";
}

function clearSimulatedAttackResults(){
	informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
}

function distanceBetween(x1, y1, x2, y2){
	var xDist = x1 - x2;
	var yDist = y1 - y2;
	return Math.sqrt((xDist*xDist) + (yDist*yDist));
}

function drawAttackDamage(damageDealt, mapFocusX, mapFocusY, attackerX, attackerY, defenderX, defenderY, isCritical){
	var floatingY = 0;
	var alpha = 1.0, 
		interval = setInterval(function() {
			var unitCoords = getUnitImageCoord(defenderX, defenderY, getTileElevation(defenderX, defenderY));
			informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
			informationCtx.fillStyle = "rgba(255, 0, 0, " + alpha + ")";
			informationCtx.font = '16px Arial';
			var damageDealtString = "";
			if (isCritical){
				damageDealtString = "-" + damageDealt.toString() + " CRITICAL!";
			}else{
				damageDealtString = "-" + damageDealt.toString();
			}
			
			informationCtx.fillText(damageDealtString, unitCoords.x-mapFocusX, unitCoords.y-mapFocusY-floatingY);
			
			alpha = alpha - 0.04;
			floatingY+=2;
			if(alpha < 0){
				clearInterval(interval);
				informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
			}
		}, FPS_DELAY);
}

function drawAttackUi(tiles, mapFocusX, mapFocusY){
	
	if (hoveredOnAttack || hoveredOnHeal){
		return;
	}
	
	var attackableOverlayImg = document.getElementById("attackable-overlay");
	
	for (var i = 0; i < tiles.length; i++){
		var gameCoord = tiles[i];
		var elevation = getTileElevation(gameCoord.x, gameCoord.y);
		var coord = getTileImageCoord(gameCoord.x, gameCoord.y, elevation);
		uiCtx.drawImage(attackableOverlayImg,// source image
                0,                     // start of x on source
                0,                     // start of y on source
                BASE_IMAGE_DIMENSIONS, // width to take from source
                BASE_IMAGE_DIMENSIONS, // height to take from source
                (coord.x - mapFocusX), // start x on canvas to draw
                (coord.y - mapFocusY), // start y on canvas to draw
                TILE_IMAGE_SIZE,            // width on canvas
                TILE_IMAGE_SIZE);           // height on canvas
	}
}

function drawHealingUi(tiles, mapFocusX, mapFocusY){
	if (hoveredOnAttack || hoveredOnHeal){
		return;
	}
	
	var attackableOverlayImg = document.getElementById("healable-overlay");
	
	for (var i = 0; i < tiles.length; i++){
		var gameCoord = tiles[i];
		var elevation = getTileElevation(gameCoord.x, gameCoord.y);
		var coord = getTileImageCoord(gameCoord.x, gameCoord.y, elevation);
		uiCtx.drawImage(attackableOverlayImg,// source image
                0,                     // start of x on source
                0,                     // start of y on source
                BASE_IMAGE_DIMENSIONS, // width to take from source
                BASE_IMAGE_DIMENSIONS, // height to take from source
                (coord.x - mapFocusX), // start x on canvas to draw
                (coord.y - mapFocusY), // start y on canvas to draw
                TILE_IMAGE_SIZE,            // width on canvas
                TILE_IMAGE_SIZE);           // height on canvas
	}
}

function drawHealthRegained(healingAmount, mapFocusX, mapFocusY, healerX, healerY, healedUnitX, healedUnitY){
	var floatingY = 0;
	var alpha = 1.0, 
		interval = setInterval(function() {
			var unitCoords = getUnitImageCoord(healedUnitX, healedUnitY, getTileElevation(healedUnitX, healedUnitY));
			informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
			informationCtx.fillStyle = "rgba(0, 255, 0, " + alpha + ")";
			informationCtx.font = '16px Arial';
			var healthRegainedString = "+" + healingAmount.toString();
			informationCtx.fillText(healthRegainedString, unitCoords.x-mapFocusX, unitCoords.y-mapFocusY-floatingY);

			alpha = alpha - 0.04;
			floatingY+=2;
			if(alpha < 0){
				clearInterval(interval);
				informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
			}
		}, FPS_DELAY);
}

function drawMapRow(mapFocusX, mapFocusY, yCoordinateToDraw){

	for(var column = 0; column < mapJson.width; column++){
		var tileIndex = (yCoordinateToDraw*mapJson.width) + column;
		var tile = mapJson.tiles[tileIndex];
		var elevation = tile.elev;
		
		var imageCoord = getTileImageCoord(column, yCoordinateToDraw, elevation);
		
		if (!SIMPLE_2D_GRAPHICS){
			mapCtx.filter = getTileBrightness(elevation);
			var tileImage = getTileImage(tile.type);
			mapCtx.drawImage(tileImage,                        // source image
			                 0,                          // start of x on source
			                 0,                          // start of y on source
			                 BASE_IMAGE_DIMENSIONS,      // width to take from source
			                 BASE_IMAGE_DIMENSIONS,      // height to take from source
			                 (imageCoord.x - mapFocusX), // start x on canvas to draw
			                 (imageCoord.y - mapFocusY), // start y on canvas to draw
			                 TILE_IMAGE_SIZE,            // width on canvas
			                 TILE_IMAGE_SIZE);           // height on canvas
			
			if(inSetupState){
				if((isLeftSidePlayer && (column < 2)) || (!isLeftSidePlayer && (column > mapJson.width-3))){
					mapCtx.drawImage(document.getElementById("spawn-overlay"),// source image
	                        0,                     // start of x on source
	                        0,                     // start of y on source
	                        BASE_IMAGE_DIMENSIONS, // width to take from source
	                        BASE_IMAGE_DIMENSIONS, // height to take from source
	                        (imageCoord.x - mapFocusX), // start x on canvas to draw
	                        (imageCoord.y - mapFocusY), // start y on canvas to draw
	                        TILE_IMAGE_SIZE,            // width on canvas
	                        TILE_IMAGE_SIZE);           // height on canvas
				}
			}
		}else{
			mapCtx.strokeRect((imageCoord.x - mapFocusX), (imageCoord.y - mapFocusY), TILE_IMAGE_SIZE, TILE_IMAGE_SIZE);
			mapCtx.fillStyle = get2dMapTileColor(tile, true);
			mapCtx.fillRect((imageCoord.x - mapFocusX), (imageCoord.y - mapFocusY), TILE_IMAGE_SIZE, TILE_IMAGE_SIZE);
			
			if (tile.type == "trees"){
				mapCtx.drawImage(document.getElementById("trees-overlay"),// source image
                        0,                     // start of x on source
                        0,                     // start of y on source
                        BASE_IMAGE_DIMENSIONS, // width to take from source
                        BASE_IMAGE_DIMENSIONS, // height to take from source
                        (imageCoord.x - mapFocusX), // start x on canvas to draw
                        (imageCoord.y - mapFocusY), // start y on canvas to draw
                        TILE_IMAGE_SIZE,            // width on canvas
                        TILE_IMAGE_SIZE);           // height on canvas
			}
		}
		
		if (mapJson.objective_coordinate_x == column && mapJson.objective_coordinate_y == yCoordinateToDraw){
			mapCtx.filter = "brightness(100%)";
			var objectiveOverlayImage;
			if(!SIMPLE_2D_GRAPHICS){
				objectiveOverlayImage = document.getElementById("objective-overlay");
			}else{
				objectiveOverlayImage = document.getElementById("objective-overlay-2d");
			}
			
			mapCtx.drawImage(objectiveOverlayImage, // source image
	                 0,                          // start of x on source
	                 0,                          // start of y on source
	                 BASE_IMAGE_DIMENSIONS,      // width to take from source
	                 BASE_IMAGE_DIMENSIONS,      // height to take from source
	                 (imageCoord.x - mapFocusX), // start x on canvas to draw
	                 (imageCoord.y - mapFocusY), // start y on canvas to draw
	                 TILE_IMAGE_SIZE,            // width on canvas
	                 TILE_IMAGE_SIZE);           // height on canvas
		}
		
		if (SIMPLE_2D_GRAPHICS){
			mapCtx.drawImage(document.getElementById("elevation-overlay-"+elevation), // source image
	                 0,                          // start of x on source
	                 0,                          // start of y on source
	                 BASE_IMAGE_DIMENSIONS,      // width to take from source
	                 BASE_IMAGE_DIMENSIONS,      // height to take from source
	                 (imageCoord.x - mapFocusX), // start x on canvas to draw
	                 (imageCoord.y - mapFocusY), // start y on canvas to draw
	                 TILE_IMAGE_SIZE,            // width on canvas
	                 TILE_IMAGE_SIZE);           // height on canvas
		}
	}
}

function drawMovementUi(moves, mapFocusX, mapFocusY, selectedX, selectedY){

	if (hoveredOnAttack || hoveredOnHeal){
		return;
	}
	
	uiCtx.fillStyle = "rgba(0, 0, 255, 0.4)";
	uiCtx.strokeStyle = "rgba(0, 0, 255, 0.4)";
	uiCtx.lineWidth = 1;
	var unitElevation = getTileElevation(selectedX, selectedY);
	
	var radius = TILE_IMAGE_SIZE / 16;

	for (var i = 0; i < moves.length; i++){
		
		var destinationCoordElevation = getTileElevation(moves[i].final_location.x, moves[i].final_location.y);
		var destinationCoordCenter = getCenterOfTile(moves[i].final_location.x, moves[i].final_location.y, destinationCoordElevation);
		
		uiCtx.beginPath();
		uiCtx.arc(destinationCoordCenter.x-mapFocusX, destinationCoordCenter.y-mapFocusY, radius, 0, 2 * Math.PI, false);
		uiCtx.fill();
		
		var startingCoord = getCenterOfTile(selectedX, selectedY, unitElevation);
		uiCtx.beginPath();
		uiCtx.moveTo(startingCoord.x-mapFocusX, startingCoord.y-mapFocusY);
		
		for (var step in moves[i].steps){
			var tileElevation = getTileElevation(moves[i].steps[step].x, moves[i].steps[step].y);
			var stepCoord = getCenterOfTile(moves[i].steps[step].x, moves[i].steps[step].y, tileElevation);
			uiCtx.lineTo(stepCoord.x-mapFocusX, stepCoord.y-mapFocusY);
		}
		
		uiCtx.stroke();
	}
}

function drawSimulatedAttackResults(attack, mapFocusX, mapFocusY){
	console.log(attack);
	var elevation = getTileElevation(attack.enemy_x, attack.enemy_y);
	var coord = getTileImageCoord((attack.enemy_x)+1, attack.enemy_y, elevation); 
	informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
	informationCtx.fillStyle = "white";
	informationCtx.fillRect(coord.x-mapFocusX, coord.y-mapFocusY, 250, 148);
	informationCtx.fillStyle = "rgba(0,0,0,1)";
	
	var textOffsetDelta = 14;
	var textOffset = textOffsetDelta;
	
	informationCtx.font = 'bold 14px Arial';
	informationCtx.fillText(attack.expected_damage_output+" damage expected", coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
	informationCtx.font = '14px Arial';
	textOffset+=textOffsetDelta;
	informationCtx.fillText("enemy " + attack.defending_unit_name + " has " + attack.defending_unit_current_health + " health", coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
	
	textOffset+=(2*textOffsetDelta);
	for(var i in attack.attack_strength_reasons){
		informationCtx.fillText(attack.attack_strength_reasons[i],  coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
		textOffset+=textOffsetDelta;
	}
	
	for (var i in attack.defense_multiplier_reasons){
		informationCtx.fillText(attack.defense_multiplier_reasons[i],  coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
		textOffset+=textOffsetDelta;
	}
	
	textOffset+=(2*textOffsetDelta);
	informationCtx.fillText(attack.critical_hit_multiplier + " critical hit damage (" + attack.critical_hit_probability + " chance)", coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
}

function drawSimulatedHealthResults(heal, mapFocusX, mapFocusY){
	var elevation = getTileElevation(heal.friendly_x, heal.friendly_y);
	var coord = getTileImageCoord((heal.friendly_x)+1, heal.friendly_y, elevation); 
	informationCtx.clearRect(0,0,informationCanvas.width, informationCanvas.height);
	informationCtx.fillStyle = "white";
	informationCtx.fillRect(coord.x-mapFocusX, coord.y-mapFocusY, 280, 30);
	informationCtx.fillStyle = "rgba(0,0,0,1)";
	
	var textOffsetDelta = 14;
	var textOffset = textOffsetDelta;
	
	informationCtx.font = 'bold 14px Arial';
	informationCtx.fillText("Heal " + heal.unit_to_heal_name + ", current health: " + heal.unit_to_heal_current_health  + "/" + heal.unit_to_heal_max_health, coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
	informationCtx.font = '14px Arial';
	textOffset+=textOffsetDelta;
	informationCtx.fillText(heal.unit_to_heal_name + " will regain " + heal.expected_heal_amount + " health", coord.x-mapFocusX, coord.y-mapFocusY+textOffset);
}

function drawUnit(unit, startingX, startingY, isOwnUnit){
	if(unit.is_alive){
		var image = getUnitImage(unit);
		var drawLeftFacingUnit = (isOwnUnit && isLeftSidePlayer) || (!isOwnUnit && !isLeftSidePlayer);
		
		drawUnitHealth((unit.current_health/unit.maximum_health), startingX, startingY, isOwnUnit);
		
		unitCtx.drawImage(image,               // source image
                         (108*animationFrame), // start of x on source
                         drawLeftFacingUnit ? 0 : BASE_UNIT_IMAGE_DIMENSIONS, // start of y on source
                         BASE_UNIT_IMAGE_DIMENSIONS, // width to take from source
                         BASE_UNIT_IMAGE_DIMENSIONS, // height to take from source
                         startingX,                  // start x on canvas to draw
                         startingY,                  // start y on canvas to draw
                         UNIT_IMAGE_SIZE,            // width on canvas
                         UNIT_IMAGE_SIZE);           // height on canvas
	}
}

function drawUnitHealth(healthPercentage, startingX, startingY, isOwnUnit){
	if(healthPercentage > .75){
		unitCtx.fillStyle = "rgba(0,173,6,.7)"
	}else if(healthPercentage > .5){
		unitCtx.fillStyle = "rgba(231,245,32,.7)";
	}else if(healthPercentage > .25){
		unitCtx.fillStyle = "rgba(245,181,32,.7)";
	}else{
		unitCtx.fillStyle = "rgba(245,32,32,.7)";
	}

	var barWidth = TILE_IMAGE_SIZE*.05;
	var padding = WIDTH*.1;
	var healthPixelLength = (WIDTH-(2*padding))*healthPercentage;

	if(isOwnUnit){
		unitCtx.strokeStyle = "rgba(0,0,0,1)";
	}else{
		unitCtx.strokeStyle = "rgba(255,0,0,1)";
	}
	
	unitCtx.strokeRect(startingX+padding, startingY, WIDTH-(2*padding), barWidth);
	unitCtx.fillRect(startingX+padding, startingY, healthPixelLength, barWidth);
}

function drawUnitRow(mapFocusX, mapFocusY, yCoordinateToDraw){
	for(var i in ownUnits){
		if(ownUnits[i].y != yCoordinateToDraw){
			continue;
		}

		drawUnit(ownUnits[i], (ownUnits[i].graphical_coord.x - mapFocusX), (ownUnits[i].graphical_coord.y - mapFocusY), true);
	}
	
	if (!inSetupState){
		for(var i in enemyUnits){
			if(enemyUnits[i].y != yCoordinateToDraw){
				continue;
			}
			
			drawUnit(enemyUnits[i], (enemyUnits[i].graphical_coord.x - mapFocusX), (enemyUnits[i].graphical_coord.y - mapFocusY), false);
		}
	}
}

function get2dMapTileColor(tile, isTopFace){
	switch(tile.type){
		case "grass":
		case "trees":
			return GRASS_COLORS[tile.elev];
		case "sand":
			return SAND_COLORS[tile.elev];
		case "rock":
			return ROCK_COLORS[tile.elev];
		case "water":
			return WATER_COLORS[tile.elev];
		default:
			return "#000000";
	}
}

//returns the pixel coordinate center of the top face of the tile
function getCenterOfTile(x, y, elevation){
	var upperLeft = getTileImageCoord(x, y, elevation);
	if(!SIMPLE_2D_GRAPHICS){
		return {'x':upperLeft.x+(TILE_IMAGE_SIZE/2), 'y':upperLeft.y+(TILE_IMAGE_SIZE-RISE-(HEIGHT/2))};
	}else{
		return {'x':upperLeft.x+(TILE_IMAGE_SIZE/2), 'y':upperLeft.y+(TILE_IMAGE_SIZE/2)};
	}
}

function getSelectedCoordinate(offsetX, offsetY, focusX, focusY){
	selectedCoordinate = {};
	var absoluteX = offsetX+focusX;
	var absoluteY = offsetY+focusY;
	if(SIMPLE_2D_GRAPHICS){
		selectedCoordinate.x = Math.floor(absoluteX/TILE_IMAGE_SIZE);
		selectedCoordinate.y = Math.floor(absoluteY/TILE_IMAGE_SIZE);
	}else{
		var possibleTiles = [];
		for(var i in centerOfTileFaces){
			if((Math.abs(centerOfTileFaces[i].center_of_face.x - absoluteX) <= TILE_IMAGE_SIZE) &&
			   (Math.abs(centerOfTileFaces[i].center_of_face.y - absoluteY) <= TILE_IMAGE_SIZE)){
				possibleTiles.push(centerOfTileFaces[i]);
			}
		}
		
		if (possibleTiles.length == 0){
			return null;
		}
		
		// TODO min distance-to-center isn't 100% accurate
		selectedCoordinate.x = possibleTiles[0].tile_coordinate.x;
		selectedCoordinate.y = possibleTiles[0].tile_coordinate.y;
		var minDistance = Math.sqrt(Math.pow((possibleTiles[0].center_of_face.x - absoluteX), 2) + (Math.pow((possibleTiles[0].center_of_face.y - absoluteY), 2)));
		for(var i = 1; i<possibleTiles.length; i++){
			newX = possibleTiles[i].center_of_face.x;
			newY = possibleTiles[i].center_of_face.y;
			var newDistance = Math.sqrt(Math.pow((newX - absoluteX), 2) + (Math.pow((newY - absoluteY), 2)));
			if(newDistance < minDistance){
				minDistance = newDistance;
				selectedCoordinate.x = possibleTiles[i].tile_coordinate.x;
				selectedCoordinate.y = possibleTiles[i].tile_coordinate.y;
			}
		}
	}
	
	return selectedCoordinate;
}

function getStepDelta(startX, startY, endX, endY){
	var stepDelta = {};

	var startElevation = getTileElevation(startX, startY);
	var startCoord = getTileImageCoord(startX, startY, startElevation);
 
	var endElevation = getTileElevation(endX, endY);
	var endCoord = getTileImageCoord(endX, endY, endElevation);
	 
	stepDelta.distance_between = distanceBetween(startCoord.x, startCoord.y, endCoord.x, endCoord.y);
	stepDelta.angle_in_radians = Math.atan2((endCoord.y - startCoord.y), (endCoord.x - startCoord.x));
	return stepDelta;
}

function getTileBrightness(tileElevation){
	switch(tileElevation){
		case 0:
			return "brightness(60%)";
		case 1:
			return "brightness(68%)";
		case 2:
			return "brightness(76%)";
		case 3:
			return "brightness(84%)";
		case 4:
			return "brightness(92%)";
		case 5:
			return "brightness(100%)";
	}
	
	console.log("no elevation of height: " + tileElevation);
	return null;
}

function getTileElevation(x, y){
	if((x < 0) || (y < 0) || (x >= mapJson.width) || (y >= mapJson.height)){
		return null;
	}
	
	if(isNaN(y) || isNaN(x)){
		return;
	}
	
	return mapJson.tiles[(mapJson.width*y)+x].elev;
}

function getTileImage(tileType){
	var image;
	switch(tileType){
		case "grass":
			image = document.getElementById("grass-tile");
			break;
		case "trees":
			image = document.getElementById("trees-tile");
			break;
		case "rock":
			image = document.getElementById("rock-tile");
			break;
		case "sand":
			image = document.getElementById("sand-tile");
			break;
		case "water":
			image = document.getElementById("water-tile");
			break;
		default:
			console.log("error getting tile image of type " + tileType);
	}
	
	return image;
}

//returns the pixel coordinate of the upper left corner of the tile image at game coordinate (x,y)
function getTileImageCoord(x, y, elevation){
	var xCoord;
	var yCoord;
	
	if(SIMPLE_2D_GRAPHICS){
		xCoord = TILE_IMAGE_SIZE*x;
		yCoord = TILE_IMAGE_SIZE*y;
	}else{
		xCoord = (WIDTH * x) + SKEW - (SKEW * y);
		yCoord = (HEIGHT * y) - ((elevation*RISE) / MAX_ELEVATION_POSSIBLE) + PADDING;
	}
	
	return {'x':xCoord, 'y':yCoord};
}

function getUnitImage(unit){
	// expands unit to, for example "desert-ranged-sprite"
	var imageId = unit.unit_faction+"-"+unit.unit_class+"-sprite";
	var image = document.getElementById(imageId);
	if(image == undefined){
		console.log("error getting unit image");
	}
	
	return image;
}

//returns the pixel coordinate of the upper left corner of the unit image
function getUnitImageCoord(x, y, elevation){
	var centerOfTile = getCenterOfTile(x, y, elevation);
	if(!SIMPLE_2D_GRAPHICS){
		return {'x':centerOfTile.x-(UNIT_IMAGE_SIZE/2), 'y':centerOfTile.y-UNIT_IMAGE_SIZE};
	}else{
		return {'x':centerOfTile.x-(UNIT_IMAGE_SIZE/2), 'y':centerOfTile.y-(UNIT_IMAGE_SIZE/2)};
	}
}

function highlightTile(coordinate){
	uiCtx.strokeRect(coordinate.x*TILE_WIDTH, coordinate.y*TILE_WIDTH, TILE_WIDTH, TILE_WIDTH);
}

function initializeGraphics(mapMessage, mapCanvasDivId, unitCanvasDivId, uiCanvasDivId, informationCanvasDivId, isRealGame){
	if(mapCanvasDivId != null){
		mapCanvas = document.getElementById(mapCanvasDivId);
		mapCtx = mapCanvas.getContext("2d");
		mapCanvas.width = CANVAS_WIDTH;
		mapCanvas.height = CANVAS_HEIGHT;
	}else{
		mapCtx = null;
	}
	
	if(unitCanvasDivId != null){
		unitCanvas = document.getElementById(unitCanvasDivId);
		unitCtx = unitCanvas.getContext("2d");
		unitCanvas.width = CANVAS_WIDTH;
		unitCanvas.height = CANVAS_HEIGHT;
	}else{
		unitCtx = null;
	}
	
	if(uiCanvasDivId != null){
		uiCanvas = document.getElementById(uiCanvasDivId);
		uiCtx = uiCanvas.getContext("2d");
		uiCanvas.width = CANVAS_WIDTH;
		uiCanvas.height = CANVAS_HEIGHT;
	}else{
		uiCtx = null;
	}
	
	if(informationCanvasDivId != null){
		informationCanvas = document.getElementById(informationCanvasDivId);
		informationCtx = informationCanvas.getContext("2d");
		informationCanvas.width = CANVAS_WIDTH;
		informationCanvas.height = CANVAS_HEIGHT;
	}else{
		informationCtx = null;
	}
	
	mapJson = mapMessage;
	resetCentersAndCoordinatesJson();
	if(isRealGame){
		inSetupState = true;
	}
}

function mapZoom(zoomingIn){
	var mapChanged = false;
	if(zoomingIn && (TILE_IMAGE_SIZE < MAX_TILE_IMAGE_SIZE)){
		mapChanged = true;
		TILE_IMAGE_SIZE+=3;
	}else if(!zoomingIn && (TILE_IMAGE_SIZE > MIN_TILE_IMAGE_SIZE)){
		mapChanged = true;
		TILE_IMAGE_SIZE-=3;
	}
	
	if(mapChanged){
		reset3dMapDimensions();
		
		//TODO there *must* be a better way to get the tile selected...
		resetCentersAndCoordinatesJson();
	}
}

function redrawMapCanvas(mapFocusX, mapFocusY){
	if(mapJson == null){
		return;
	}
	
	mapCtx.filter = "brightness(100%)";
	// While clearRect is more efficient than fillRect, I don't want a white background
	mapCtx.fillStyle = "#353535";
	mapCtx.fillRect(0,0, CANVAS_WIDTH, CANVAS_HEIGHT);
	
	for(var y = 0; y < mapJson.height; y++){
		drawMapRow(mapFocusX, mapFocusY, y);	
	}
}

function redrawUiCanvas(mapFocusX, mapFocusY){
	if(uiCtx == null){
		return;
	}
	
    uiCtx.clearRect(0, 0, uiCanvas.width, uiCanvas.height);
	
	drawMovementUi(getPossibleMoves(), mapFocusX, mapFocusY, getSelectedTileX(), getSelectedTileY());
	drawAttackUi(getAttackableTiles(), mapFocusX, mapFocusY);
	drawHealingUi(getHealableTiles(), mapFocusX, mapFocusY);
	
	if ((hoveredCoordinate != null) && (hoveredCoordinate.x != undefined) && (hoveredCoordinate.y != undefined) && !hoveredOnAttack && !hoveredOnHeal){
		var hoveredTileElevation = getTileElevation(hoveredCoordinate.x, hoveredCoordinate.y);
		var hoveredCoord = getTileImageCoord(hoveredCoordinate.x, hoveredCoordinate.y, hoveredTileElevation);

		uiCtx.drawImage(document.getElementById("mouse-hover"),
				0,
				0,
				BASE_IMAGE_DIMENSIONS,
				BASE_IMAGE_DIMENSIONS,
				(hoveredCoord.x - mapFocusX),
				(hoveredCoord.y - mapFocusY),
				TILE_IMAGE_SIZE,
                TILE_IMAGE_SIZE);
	}
}

function redrawUnitCanvas(mapFocusX, mapFocusY){
	if (unitCtx == null){
		return;
	}
	
	unitCtx.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
	
	for(var y = 0; y < mapJson.height; y++){
		drawUnitRow(mapFocusX, mapFocusY, y);
	}
}

function refreshGraphicalUnitCoords(){
	for(var i in ownUnits){
		var currentElevation = getTileElevation(ownUnits[i].x, ownUnits[i].y);
		ownUnits[i].graphical_coord = getUnitImageCoord(ownUnits[i].x, ownUnits[i].y, currentElevation);
	}
	
	if (enemyUnits.length > 0){
		for(var i in enemyUnits){
			var currentElevation = getTileElevation(enemyUnits[i].x, enemyUnits[i].y);
			enemyUnits[i].graphical_coord = getUnitImageCoord(enemyUnits[i].x, enemyUnits[i].y, currentElevation);
		}
	}
}

//calculates the centers of the parallelograms that make up the 
// "top face" of the 3D tiles, given that the first tile is at (0,0)
function resetCentersAndCoordinatesJson(){
	centerOfTileFaces = [];
	for(var row = 0; row < mapJson.height; row++){
		for(var column = 0; column < mapJson.width; column++){
			var tileIndex = (row*mapJson.width) + column;
			var elevation = mapJson.tiles[tileIndex].elev;
			var centerCoords = getCenterOfTile(column, row, elevation);
			
			var tileCoordinate = {};
			tileCoordinate.x = column;
			tileCoordinate.y = row;
			var centerOfFace = {};
			centerOfFace = centerCoords;
			var arrayEntry = {};
			arrayEntry.tile_coordinate = tileCoordinate;
			arrayEntry.center_of_face = centerOfFace;
			centerOfTileFaces.push(arrayEntry);
		}
	}
}

//used during map creation
function setMapJson(newMap){
	mapJson = newMap;
	resetCentersAndCoordinatesJson();
}
