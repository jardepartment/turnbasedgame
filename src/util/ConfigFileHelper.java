package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFileHelper {
	
	private static final String CONFIG_FILE_LOCATION = "../config.properties";
	
	private static final String OFFLINE_MAP_1_FILE = "offlinemap1.json";
	private static final String OFFLINE_MAP_2_FILE = "offlinemap2.json";
	
	private static final String FACTION_CONFIG_JSON = "factionconfig.json";
	
	private static boolean databaseOnline = false;
	
	private static String databaseUrl;
	private static String databaseUsername;
	private static String databasePassword;
	
	private static String resourcesDirectory;
	
	private ConfigFileHelper() {
		// Not constructable
	}
	
	static {
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream configInputStream = classLoader.getResourceAsStream(CONFIG_FILE_LOCATION);
			properties.load(configInputStream);
			databaseOnline = properties.getProperty("databaseonline").equalsIgnoreCase("true");
			databaseUrl = properties.getProperty("databaseurl");
			databaseUsername = properties.getProperty("databaseusername");
			databasePassword = properties.getProperty("databasepassword");
			resourcesDirectory = properties.getProperty("resourcesdirectory");
			configInputStream.close();
		} catch (IOException e) {
			System.out.println("ERROR - failed to load properties file!");
		}
		
		/* not necessarily errors if properties wern't specified. Only a subset of them 
		   should be used in different configurations: see 'databaseonline' */
		
		if(databaseUrl == null) {
			System.out.println("WARNING- 'databaseuseurl' property not set");
		}
		
		if(databaseUsername == null) {
			System.out.println("WARNING- 'databaseusername' property not set");
		}
		
		if(databasePassword == null) {
			System.out.println("WARNING- 'databasepassword' property not set");
		}
		
		if(resourcesDirectory == null) {
			System.out.println("WARNING- 'resourcesdirectory' property not set");
		}
	}
	
	public static boolean databaseOnline() {
		return databaseOnline;
	}

	public static String getDatabasePassword() {
		if(!databaseOnline) {
			System.out.println("WARNING- database is offline, should not be accessing database password property");
		}
		
		return databasePassword;
	}

	public static String getDatabaseUrl() {
		if(!databaseOnline) {
			System.out.println("WARNING- database is offline, should not be accessing database URL property");
		}
		
		return databaseUrl;
	}

	public static String getDatabaseUsername() {
		if(!databaseOnline) {
			System.out.println("WARNING- database is offline, should not be accessing database username property");
		}
		
		return databaseUsername;
	}

	public static String getOfflineMap1Path() {
		if(databaseOnline) {
			System.out.println("WARNING- database is online, offline maps should only be used when not connecting to a database");
		}
		
		return resourcesDirectory+OFFLINE_MAP_1_FILE;
	}

	public static String getOfflineMap2Path() {
		if(databaseOnline) {
			System.out.println("WARNING- database is online, offline maps should only be used when not connecting to a database");
		}
		
		return resourcesDirectory+OFFLINE_MAP_2_FILE;
	}

	public static String getFactionConfigFile() {
		return resourcesDirectory+FACTION_CONFIG_JSON;
	}
}
