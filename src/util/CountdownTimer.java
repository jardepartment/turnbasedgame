package util;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CountdownTimer<T> implements Runnable {
	
	private static final int ONE_SECOND_DELAY = 1000;
	
	private int numberOfSeconds;// number of times callback should be called
	private Consumer<T> beforeCountdownCallback; // Run once prior to starting the countdown
	private BiConsumer<T, Integer> repeatedCallback; // Run on each iteration of the countdown
	private Consumer<T> afterCountdownCallback; // Run once after the countdown ends
	private T instanceOfCaller;
	
	public CountdownTimer(int numberOfSeconds, 
	                      Consumer<T> beforeCountdownCallback,
	                      BiConsumer<T, Integer> repeatedCallback,
	                      Consumer<T> afterCountdownCallback,
	                      T instance) {
		
		this.numberOfSeconds = numberOfSeconds;
		this.beforeCountdownCallback = beforeCountdownCallback;
		this.repeatedCallback = repeatedCallback;
		this.afterCountdownCallback = afterCountdownCallback;
		this.instanceOfCaller = instance;
	}
	
	@Override
	public void run() {
		
		if(beforeCountdownCallback != null) {
			beforeCountdownCallback.accept(instanceOfCaller);
		}
		
		for(int i = 0; i < numberOfSeconds; i++) {
			if(repeatedCallback != null) {
				repeatedCallback.accept(instanceOfCaller, numberOfSeconds - i);
			}

			try {
				Thread.sleep(ONE_SECOND_DELAY);
			} catch (InterruptedException e) {
				System.out.println("Countown thread interrupted");
				return;
			}
		}
		
		if(afterCountdownCallback != null) {
			afterCountdownCallback.accept(instanceOfCaller);
		}
	}
}
