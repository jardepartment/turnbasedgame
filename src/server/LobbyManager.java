package server;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.Session;

import map.BattleArea;
import server.dao.OfflineQueryHandler;
import server.dao.PostgresQueryHandler;
import server.dao.QueryHandler;
import util.ConfigFileHelper;

public class LobbyManager {
	
	private Map<Integer, Lobby> lobbies;
	private Map<String, User> users;
	private QueryHandler queryHandler;
	
	LobbyManager(){
		lobbies = new TreeMap<Integer, Lobby>();
		users = new HashMap<String, User>();
		if (ConfigFileHelper.databaseOnline()) {
			queryHandler = new PostgresQueryHandler();
		} else {
			queryHandler = new OfflineQueryHandler();
		}
	}
	
	public void deleteLobby(int lobbyId) {
		lobbies.remove(lobbyId);
	}

	public JsonArray getLobbies(String lobbyType) {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		for(Lobby l : lobbies.values()) {
			builder.add(l.getLobbyAsJsonObject());
		}
		
		return builder.build();
	}

	public void handleMessage(JsonObject message, Session session) {
		switch(message.getString("message_id")) {
			case "user_login":
				login(message, session);
				break;
			case "faction_selected":
				users.get(session.getId()).setFaction(message.getString("faction"));
				break;
			case "browse_lobbies":
				browseLobbies(message, session);
				break;
			case "join_lobby":
				System.out.println("joining lobby");
				handleJoinLobby(message, session);
				break;
			case "leave_lobby":
				System.out.println("leaving lobby");
				removeUserFromLobby(session.getId());
				break;
			case "create_lobby":
				System.out.println("creating lobby");
				createLobby(message, session);
				break;
			case "search_maps":
				System.out.println("searching for maps");
				handleMapSearch(message, session);
				break;
			case "selected_map":
				handleMapSelect(message, session);
				break;
			case "random_map_select":
				handleRandomMapVote(message, session);
				break;
			case "save_map":
				saveMap(users.get(session.getId()), message.getJsonObject("map"));
				break;
			default:
				System.out.println("Lobby - unknown message received");
		}
	}
	
	public void removeUserFromLobby(String sessionId) {
		User userToRemove = users.get(sessionId);
		if(userToRemove == null) {
			return;
		}
		
		for(Lobby l : lobbies.values()) {
			if(l.removeUser(sessionId)) {
				System.out.println("REMOVING USER from lobby: " + l.getId());
				if(l.isEmpty()) {
					lobbies.remove(l.getId());
				}
				
				break;
			}
		}
	}

	private boolean addUserToLobby(JsonObject message, Session session) {
		int lobbyId = message.getInt("lobby_id");
		User joiningUser = users.get(session.getId());
		if(lobbies.containsKey(lobbyId)) {
			lobbies.get(lobbyId).addUser(joiningUser);
			return true;
		}else {
			System.out.println("Error joining lobby - ID " + lobbyId + " no longer exists");
			return false;
		}
	}

	private void browseLobbies(JsonObject message, Session session) {
		JsonObject lobbies = Json.createObjectBuilder()
				.add("message_id", "lobby_list_update")
				.add("lobbies", getLobbies(message.getString("lobby_type")))
				.build();
		ServerSocket.sendJsonObject(session, lobbies);
	}

	private void connectUser(User user) {
		JsonObjectBuilder loginResponse = Json.createObjectBuilder();
		loginResponse.add("message_id", "login_response");
		
		if(!users.containsKey(user.getSession().getId())) {
			users.put(user.getSession().getId(), user);
			loginResponse.add("is_successful", true);
			loginResponse.add("username", user.getUsername());
		} else {
			loginResponse.add("is_successful", false);
		}
		
		ServerSocket.sendJsonObject(user.getSession(), loginResponse.build());
	}

	private void createLobby(JsonObject message, Session session) {
		int lobbyId = (int)(Math.random()*10000);
		while(this.lobbies.containsKey(lobbyId)) {
			lobbyId = (int)(Math.random()*10000);
		}
		
		User creatingUser = users.get(session.getId());
		Lobby lobby = new Lobby(lobbyId);
		lobby.setBattleArea(message.getJsonObject("map"));
		lobby.addUser(creatingUser);
		System.out.println(lobbyId);
		lobbies.put(lobbyId, lobby);
	}

	private void handleJoinLobby(JsonObject message, Session session) {
		JsonObjectBuilder joinLobbyResponse = Json.createObjectBuilder();
		joinLobbyResponse.add("message_id", "join_lobby_response");
		if(addUserToLobby(message, session)) {
			joinLobbyResponse.add("is_successful", true);
		} else {
			joinLobbyResponse.add("is_successful", false);
		}
		
		ServerSocket.sendJsonObject(session, joinLobbyResponse.build());
	}
	
	private void handleMapSearch(JsonObject message, Session session) {
		ServerSocket.sendJsonObject(session, Json.createObjectBuilder()
				.add("message_id", "map_search_results")
				.add("maps", queryHandler.getMapResults(message)).build());
	}
	
	private void handleMapSelect(JsonObject message, Session session) {
		int lobbyId = message.getInt("lobby_id");
		lobbies.get(lobbyId).handleMapVote(session, queryHandler.getMapById(message.getInt("map_id")), false);
	}
	
	private void handleRandomMapVote(JsonObject message, Session session) {
		int lobbyId = message.getInt("lobby_id");
		//TODO size should be determined by army size
		lobbies.get(lobbyId).handleMapVote(session, queryHandler.getRandomMap("medium"), true);
	}

	private void login(JsonObject message, Session session) {
		String username = message.getString("username");
		User user = new User(username, session);
		connectUser(user);
	}

	private void saveMap(User user, JsonObject map) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("message_id", "save_map_response");
		
		try {
			builder.add("response", queryHandler.insertMap(user, new BattleArea(map)));
		} catch(Exception e) {
			System.out.println("exception thrown on map insert");
			e.printStackTrace();
			builder.add("response", Json.createObjectBuilder().add("success", false).add("message", "malformed map").build());
		}
		
		ServerSocket.sendJsonObject(user.getSession(), builder.build());
	}
}
