package server.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import map.Grid;
import server.User;
import util.ConfigFileHelper;

public class OfflineQueryHandler implements QueryHandler {
	
	private JsonArray testingMaps;
	
	public OfflineQueryHandler() {
		try {
			File map1 = new File(ConfigFileHelper.getOfflineMap1Path());
			File map2 = new File(ConfigFileHelper.getOfflineMap2Path());
			InputStream is1 = new FileInputStream(map1);
			InputStream is2 = new FileInputStream(map2);
			JsonReader r1 = Json.createReader(is1);
			JsonReader r2 = Json.createReader(is2);
			testingMaps = Json.createArrayBuilder()
					.add(r1.readObject())
					.add(r2.readObject()).build();
			r1.close();
			r2.close();
			is1.close();
			is2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public JsonObject getMapById(int id) {
		for(int i = 0; i < testingMaps.size(); i++) {
			JsonObject mapJson = testingMaps.getJsonObject(i);
			if(mapJson.getInt("id") == id) {
				return mapJson;
			}
		}
		
		System.out.println("no map found with ID: " + id);
		return null;
	}

	@Override
	public JsonArray getMapResults(JsonObject criteriaJson){
		// ignoring criteria, return all the offline maps
		return this.testingMaps;
	}
	
	@Override
	public JsonObject getRandomMap(String sizeCrioteria) {
		// 'random' will always return id 2 when no database is connected
		return getMapById(2);
	}
	
	@Override
	public JsonObject insertMap(User user, Grid<?> map) {
		System.out.println("--OFFLINE MAP SAVE--");
		System.out.println(map.getMapAsJson());
		return Json.createObjectBuilder()
				.add("success", false)
				.add("message", "Failed to save map - No database connection").build();
	}
}
