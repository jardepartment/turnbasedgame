package server.dao;

import java.io.StringReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import map.BattleTile;
import map.Grid;
import server.User;
import util.ConfigFileHelper;

public class PostgresQueryHandler implements QueryHandler {
	
	private Connection connection;
	
	public PostgresQueryHandler() {
		try {
			DriverManager.registerDriver(new org.postgresql.Driver());
			connection = DriverManager.getConnection(ConfigFileHelper.getDatabaseUrl(), ConfigFileHelper.getDatabaseUsername(), ConfigFileHelper.getDatabasePassword());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public JsonObject getMapById(int id) {
		String sqlStatement = "SELECT * FROM \"MAP\" where \"MAP\".\"ID\" = " + id +";";
		try {
			ResultSet results = connection.createStatement().executeQuery(sqlStatement);
			return convertMapResultSetToJson(results).getJsonObject(0);
		} catch (SQLException e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public JsonArray getMapResults(JsonObject criteriaJson) {
		String creatorCriteria = null;
		StringBuilder queryString = new StringBuilder().append("SELECT * FROM \"MAP\"");
		
		if (criteriaJson.containsKey("creator_criteria")) {
			creatorCriteria = criteriaJson.getString("creator_criteria");
		}
		
		String sizeCriteria = criteriaJson.getString("size_criteria");
		// start with all possible ranges
		int lowerBound = Grid.TILE_AREA_SMALL_LB;
		int upperBound = Grid.TILE_AREA_X_LARGE_UB;
		
		if (sizeCriteria.equals("small")) {
			lowerBound = Grid.TILE_AREA_SMALL_LB;
			upperBound = Grid.TILE_AREA_SMALL_UB;
		} else if (sizeCriteria.equals("medium")) {
			lowerBound = Grid.TILE_AREA_MEDIUM_LB;
			upperBound = Grid.TILE_AREA_MEDIUM_UB;
		} else if (sizeCriteria.equals("large")) {
			lowerBound = Grid.TILE_AREA_LARGE_LB;
			upperBound = Grid.TILE_AREA_LARGE_UB;
		} else if (sizeCriteria.equals("x-large")){ // XL
			lowerBound = Grid.TILE_AREA_X_LARGE_LB;
			upperBound = Grid.TILE_AREA_X_LARGE_UB;
		}
		
		queryString.append(" WHERE (\"WIDTH\" * \"HEIGHT\") >= ").append(lowerBound);
		queryString.append(" AND (\"WIDTH\" * \"HEIGHT\") <= ").append(upperBound);
		
		if (creatorCriteria != null) {
			queryString.append(" AND LOWER(\"CREATOR\") LIKE LOWER('").append(creatorCriteria).append("%')").toString(); 
		}
		
		String sqlStatement = queryString.toString();
		try {
			ResultSet results = connection.createStatement().executeQuery(sqlStatement);
			return convertMapResultSetToJson(results);
		} catch (SQLException e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public JsonObject getRandomMap(String sizeCriteria) {
		JsonObject criteriaJson = Json.createObjectBuilder().add("size_criteria", sizeCriteria).build();
		JsonArray maps = getMapResults(criteriaJson);
		int randomIndex = (int)(Math.random() * maps.size());
		return maps.getJsonObject(randomIndex);
	}
	
	
	@Override
	public JsonObject insertMap(User user, Grid<?> map) {
		JsonObject tiles = Json.createObjectBuilder().add("tiles", map.getMapAsJson().getJsonArray("tiles")).build();
		String creator = user.getUsername();
		
		for (JsonValue mapResultsJson : getAllMaps()) {
			Grid<BattleTile> g = new Grid<BattleTile>(mapResultsJson.asJsonObject());
			if(g.equals(map)) {
				return Json.createObjectBuilder()
						.add("success", false)
						.add("message", "Failed to save map - Map already exists in the database! Created by " + mapResultsJson.asJsonObject().getString("creator")).build();
			}
		}
		
		//TODO Update to PreparedStatement
		String insertStatement = "INSERT INTO \"MAP\" (\"WIDTH\", \"HEIGHT\", \"OBJ_X\", \"OBJ_Y\", \"CREATOR\", \"TILES\") VALUES ("
				+ map.getWidth() + ", " 
				+ map.getHeight() + ", " 
				+ map.getObjectiveCoordinate().x + ", " 
				+ map.getObjectiveCoordinate().y + ", '" 
				+ creator + "', '" 
				+ tiles.toString() + "');";
		try {
			connection.createStatement().executeUpdate(insertStatement);
			return Json.createObjectBuilder()
					.add("success", true)
					.add("message", "Map saved successfully").build();
		} catch (SQLException e){
			e.printStackTrace();
			return Json.createObjectBuilder()
					.add("success", false)
					.add("message", "Failed to save map - SQL exception thrown").build();
		}
	}
	
	private JsonArray convertMapResultSetToJson(ResultSet mapResults) throws SQLException{
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		while(mapResults.next()) {
			
			JsonReader reader = Json.createReader(new StringReader(mapResults.getString("TILES")));
			JsonObject tiles = reader.readObject();
			reader.close();
			
			arrayBuilder.add(Json.createObjectBuilder()
					.add("id", mapResults.getInt("ID"))
					.add("creator", mapResults.getString("CREATOR"))
					.add("objective_coordinate_x", mapResults.getInt("OBJ_X"))
					.add("objective_coordinate_y", mapResults.getInt("OBJ_Y"))
					.add("width", mapResults.getInt("WIDTH"))
					.add("height", mapResults.getInt("HEIGHT"))
					.add("tiles", tiles.getJsonArray("tiles"))
					.build());
		}
		
		return arrayBuilder.build();
	}

	private JsonArray getAllMaps() {
		String sqlStatement = "SELECT * FROM \"MAP\"";
		try {
			ResultSet results = connection.createStatement().executeQuery(sqlStatement);
			return convertMapResultSetToJson(results);
		} catch (SQLException e){
			e.printStackTrace();
			return null;
		}
	}
}
