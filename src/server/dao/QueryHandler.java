package server.dao;

import javax.json.JsonArray;
import javax.json.JsonObject;

import map.Grid;
import server.User;

public interface QueryHandler {
	
	JsonObject getMapById(int id);

	JsonArray getMapResults(JsonObject criteriaJson);
	
	JsonObject getRandomMap(String sizeCriteria);
	
	JsonObject insertMap(User user, Grid<?> map);
}
