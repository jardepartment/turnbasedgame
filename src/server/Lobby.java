package server;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.Session;

import battle.Battle;
import util.CountdownTimer;

public class Lobby {
	
	private static final int COUNTDOWN_DURATION = 10;
	
	private int id;
	private User leftUser;
	private User rightUser;
	
	private boolean leftVotedMapRandom;
	private boolean rightVotedMapRandom;
	private JsonObject leftVotedMap;
	private JsonObject rightVotedMap;
	private JsonObject mapJson;
	private AtomicBoolean countdownStarted;
	private String password = null;
	private Thread countdown;
	
	private boolean leftMapChosen;
	
	public Lobby(int lobbyId){
		this.id = lobbyId;
		leftUser = null;
		rightUser = null;
		
		leftVotedMap = null;
		rightVotedMap = null;
		
		leftVotedMapRandom = false;
		rightVotedMapRandom = false;
		
		this.leftMapChosen = false;
		this.countdownStarted = new AtomicBoolean(false);
		
		countdown = new Thread(new CountdownTimer<Lobby>(COUNTDOWN_DURATION,
		                                                 Lobby::setCountdownStarted,
		                                                 Lobby::sendGameStartingCountdownMessage,
		                                                 Lobby::startGame,
		                                                 this));
	}
/*	
	//TODO implement
	public Lobby(int lobbyId, String password){
		this.id = lobbyId;
		this.password = password;
		this.connectedUsers = new HashMap<String, User>();
	}*/
	
	public void addUser(User user) {
		
		if(isFull()){
			System.out.println("lobby is full");
			sendFailureToAdd(user, "lobby is full");
			return;
		}else if(((rightUser != null) && rightUser.equals(user)) || ((leftUser != null) && leftUser.equals(user))){
			System.out.println("user with session ID '" + user.getSession().getId() + "' already exists in lobby");
			sendFailureToAdd(user, "non-unique session ID");
			return;
		}else{
			if(leftUser == null) {
				leftUser = user;
			}else{
				rightUser = user;
			}
			
			sendLobbyUpdate();
			broadCastSelectedMapVotes();
			System.out.println("connecting user: " + user.getUsername() + " - " + user.getSession().getId());
			
			if(isReadyToStart() && !countdownStarted.get()) {
				this.countdown.start();
		    }
		}
	}

	public void broadCastSelectedMapVotes() {
		JsonObjectBuilder mapVoteJson = Json.createObjectBuilder();
		mapVoteJson.add("message_id", "map_vote_broadcast");
		if(leftVotedMap != null) {
			if(leftVotedMapRandom) {
				mapVoteJson.add("left_map", "random");
			}else {
				mapVoteJson.add("left_map", leftVotedMap);
			}
		}
		
		if(rightVotedMap != null) {
			if(rightVotedMapRandom) {
				mapVoteJson.add("right_map", "random");
			}else {
				mapVoteJson.add("right_map", rightVotedMap);
			}
		}
		System.out.println("sending broadcast");
		
		JsonObject message = mapVoteJson.build();
		if (leftUser != null) {
			ServerSocket.sendJsonObject(leftUser.getSession(), message);
		}
		
		if (rightUser != null) {
			ServerSocket.sendJsonObject(rightUser.getSession(), message);
		}
	}

	public int getId() {
		return this.id;
	}
	
	public JsonObject getLobbyAsJsonObject() {
		return Json.createObjectBuilder()
				.add("lobby_id", this.id)
				.add("left_lobby_member", leftUser.getUserAsJsonObject())
				.build();
	}

	public void handleMapVote(Session session, JsonObject mapJson, boolean isRandom) {
		if(leftUser.getSession().equals(session)) {
			leftVotedMap = mapJson;
			if(isRandom) {
				leftVotedMapRandom = true;
			}else {
				leftVotedMapRandom = false;
			}
		}else if(rightUser.getSession().equals(session)) {
			rightVotedMap = mapJson;
			if(isRandom) {
				rightVotedMapRandom = true;
			}else {
				rightVotedMapRandom = false;
			}
		}else {
			System.out.println("ERROR - setVotedMap() - No session matching " + session.getId());
		}
		
		broadCastSelectedMapVotes();
		
		if (!isFull()) {
			return;
		}
		
		if(isReadyToStart() && (!countdownStarted.get())) {
			this.countdown.start();
		}
	}

	public boolean isEmpty() {
		return ((leftUser == null) && (rightUser == null));
	}
	
	public boolean isFull() {
		return ((leftUser != null) && (rightUser != null));
	}

	public boolean isPasswordProtected() {
		return (password == null) ? false : true;
	}

	public boolean removeUser(String sessionId) {
		boolean removed = false;
		if((rightUser != null) && (rightUser.getSession().getId().equals(sessionId))) {
			removed = true;
			rightVotedMap = null;
			rightVotedMapRandom = false;
			rightUser = null;
		}else if((leftUser != null) && (leftUser.getSession().getId().equals(sessionId))) {
			//TODO leftuser leaving lobby does not correctly transfer map votes to the right user
			removed = true;
			leftVotedMap = rightVotedMap;
			leftVotedMapRandom = rightVotedMapRandom;
			rightVotedMap = null;
			rightVotedMapRandom = false;
			leftUser = rightUser;
			rightUser = null;
		}
		
		if(removed && countdownStarted.get()) {
			countdown.interrupt();
			countdownStarted.set(false);
			countdown = new Thread(new CountdownTimer<Lobby>(COUNTDOWN_DURATION, 
			                                                 Lobby::setCountdownStarted,
			                                                 Lobby::sendGameStartingCountdownMessage,
			                                                 Lobby::startGame,
			                                                 this));
		}
			
		sendLobbyUpdate();
		
		return removed;
	}

	public void setBattleArea(JsonObject mapJson) {
		this.mapJson = mapJson;
	}
	
	private void chooseMap() {
		if(leftVotedMap == null){
			setBattleArea(rightVotedMap);
		}else if(rightVotedMap == null) {
			setBattleArea(leftVotedMap);
			this.leftMapChosen = true;
		}else{
			if(Math.random() < .5) {
				setBattleArea(leftVotedMap);
				this.leftMapChosen = true;
			}else{
				setBattleArea(rightVotedMap);
			}
		}
		
		if(this.mapJson == null) {
			throw new RuntimeException("map unable to be chosen!");
		}
		
		JsonObject mapPickedMessage = Json.createObjectBuilder()
				.add("message_id", "map_picked")
				.add("left_map_picked", this.leftMapChosen).build();
		
		ServerSocket.sendJsonObject(leftUser.getSession(), mapPickedMessage);
		ServerSocket.sendJsonObject(rightUser.getSession(), mapPickedMessage);
	}

	private void createNewBattle() {
		Battle battle = new Battle(this.mapJson, leftUser, rightUser);
		ServerSocket.currentBattles.put(this.id, battle);
		battle.initializeMapAndUnits(this.id);
	}

	private boolean isReadyToStart() {
		if (!isFull()) {
			return false;
		}
		
		boolean atLeastOneVoted = false;
		if((leftVotedMap != null) || (rightVotedMap != null)) {
			atLeastOneVoted = true;
		}
		
		return atLeastOneVoted;
	}

	private void sendFailureToAdd(User user, String reason) {
		JsonObject object = Json.createObjectBuilder()
				.add("message_id", "failed_to_add")
				.add("reason", reason)
				.build();
		ServerSocket.sendJsonObject(user.getSession(), object);
	}

	private void sendGameStartingCountdownMessage(int secondsRemaining) {
		JsonObject currentCountdown = Json.createObjectBuilder().add("message_id", "game_starting_countdown").add("seconds_left", secondsRemaining).build();
		
		if(leftUser != null) {
			ServerSocket.sendJsonObject(leftUser.getSession(), currentCountdown);
		}
		
		if(rightUser != null) {
			ServerSocket.sendJsonObject(rightUser.getSession(), currentCountdown);
		}
	}

	private void sendLobbyUpdate() {
		JsonArrayBuilder userArrayBuilder = Json.createArrayBuilder();
		if (leftUser != null) {
			userArrayBuilder.add(leftUser.getUserAsJsonObject());
		}
		
		if (rightUser != null) {
			userArrayBuilder.add(rightUser.getUserAsJsonObject());
		}
		
		JsonArray userArray = userArrayBuilder.build();
		
		if (leftUser != null) {
			ServerSocket.sendJsonObject(leftUser.getSession(), Json.createObjectBuilder()
					.add("message_id", "lobby_member_update")
					.add("lobby_id", this.id)
					.add("lobby_members", userArray)
					.add("is_left_side", true)
					.build());
		}
		
		if (rightUser != null) {
			ServerSocket.sendJsonObject(rightUser.getSession(), Json.createObjectBuilder()
					.add("message_id", "lobby_member_update")
					.add("lobby_id", this.id)
					.add("lobby_members", userArray)
					.add("is_left_side", false)
					.build());
		}
	}

	private void setCountdownStarted() {
		this.countdownStarted.set(true);
	}

	private void startGame() {
		if(!isFull()) {
			System.out.println("Can't call startGame() if the lobby is not full");
			return;
		}
		
		Thread battleInitializer = new Thread(new CountdownTimer<Lobby>(2, 
		                                                                Lobby::chooseMap,
		                                                                null,
		                                                                Lobby::createNewBattle,
		                                                                this));
		battleInitializer.start();

	}
}
