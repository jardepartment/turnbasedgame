package server;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import battle.Battle;
import battle.unit.UnitGenerator;
import util.JsonEncoder;

@ServerEndpoint(value="/endpoint", encoders = {JsonEncoder.class})
public class ServerSocket {

	private static Set<Session> connectedSessions = new HashSet<Session>();
	private static LobbyManager lobbyManager = new LobbyManager();
	public static Map<Integer, Battle> currentBattles = new HashMap<Integer, Battle>();
	
	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Open Connection from session: " + session.getId());
		connectedSessions.add(session);
		broadcastUpdatedConnectionCount();
		if( UnitGenerator.getParsedFactionsJsonArray() ==  null) {
			System.out.println("Failed to parse units JSON array - ensure resourcesdirectory is set correctly in config.properties");
		}else {
			sendJsonObject(session, Json.createObjectBuilder().add("message_id", "unit_config").add("config", UnitGenerator.getParsedFactionsJsonArray()).build());
		}
	}
	
	
	@OnClose
	public void onClose(Session session) {
		connectedSessions.remove(session);
		broadcastUpdatedConnectionCount();
		try {
			lobbyManager.removeUserFromLobby(session.getId());
			System.out.println("Closed connection from session: " + session.getId());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@OnMessage
	public void onMessage(String message, Session session) {
		System.out.println("message from client: " + message);
		JsonReader reader = Json.createReader(new StringReader(message));
		JsonObject messageObject = reader.readObject();
		reader.close();
		
		switch(messageObject.getString("message_type")) {
		case "lobby":
			lobbyManager.handleMessage(messageObject, session);
			break;
		case "battle":
			System.out.println(messageObject.toString());
			int battleId = messageObject.getInt("battle_id");
			if(currentBattles.containsKey(battleId)) {
				currentBattles.get(battleId).handleMessage(messageObject, session);
			}else {
				System.out.println("Error - no battle ID matching " + battleId);
			}
			
			if (messageObject.getString("message_id").equals("game_over") || 
				messageObject.getString("message_id").equals("player_left_battle")) {
				lobbyManager.removeUserFromLobby(session.getId());
				currentBattles.remove(battleId);
			}
			break;
		default:
			System.out.println("server - unknown message type received");
		}
	}
	
	
	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}
	
	// sends a json object to a client
	public synchronized static void sendJsonObject(Session session, JsonObject object) {
		try {
			session.getBasicRemote().sendObject(object);
		} catch (IOException | EncodeException e) {
			e.printStackTrace();
		}
	}

	private void broadcastUpdatedConnectionCount() {
		int totalCount = connectedSessions.size();
		JsonObject connectionUpdate = Json.createObjectBuilder()
				.add("message_id", "player_count_broadcast")
				.add("count", totalCount).build();
		for (Session s : connectedSessions) {
			sendJsonObject(s, connectionUpdate);
		}
	}
}
