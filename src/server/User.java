package server;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.Session;

import battle.Army;

public class User {
	
	private String username;
	private Session session;
	private Army army;
	private String faction;
	
	public User(String username, Session session) {
		this.username = username;
		this.session = session;
		this.army = new Army();
		this.faction = "desert";
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof User)) {
			return false;
		}
		
		User u = (User)o;
		
		return (this.session.getId().equals(u.getSession().getId()));
	}

	public Army getArmy() {
		return this.army;
	}
	
	public String getFaction() {
		return this.faction;
	}

	public Session getSession(){
		return this.session;
	}

	public JsonObject getUserAsJsonObject() {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("name", this.username);
		return builder.build();
	}

	public String getUsername() {
		return this.username;
	}
	
	@Override
	public int hashCode() {
		return session.hashCode();
	}

	public void setArmy(Army army) {
		this.army = army;
	}
	
	public void setFaction(String faction) {
		this.faction = faction;
	}
	
	public void resetArmy() {
		this.army.deleteAllUnits();
		this.army.setIsReady(false);
	}

}
