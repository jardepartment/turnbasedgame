package battle.unit;

import java.util.HashMap;
import java.util.Map;

// thin wrapper around a Map interface + built-in default values
public class CombatMultipliers<T> {
	
	private Map<T, Double> bonuses;
	private double DEFAULT_PERCENT_BONUS = 0.0;
	
	public CombatMultipliers() {
		bonuses = new HashMap<T, Double>();
	}
	
	public double getPercentBonus(T entity) {
		if(bonuses.containsKey(entity)) {
			return bonuses.get(entity);
		}
		
		//default if bonus hasn't been set for this type
		return DEFAULT_PERCENT_BONUS;
	}

	public void setPercentBonus(T entity, double multiplier) {
		this.bonuses.put(entity, multiplier);
	}
}
