package battle.unit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.json.JsonObject;

import map.BattleArea;
import map.BattleTile;
import map.Coordinate;

public class CombatTraits {
	
	
	public final static Set<String> ELEVATION_DELTA_TYPES = new HashSet<String>(Arrays.asList("uphill", "downhill", "level"));
	
	private JsonObject combatTraitsJson;
	private double baseAttackStrength;
	private double maximumRange;
	private int maximumElevationDelta;
	private double criticalHitProbability;
	private double criticalHitMultiplier;
	
	// Attacker multipliers
	private CombatMultipliers<String> attackingAgainstUnitMultipliers;
	private CombatMultipliers<String> attackingWithElevationDeltaMultipliers;
	
	// Defender multipliers
	private CombatMultipliers<String> defendingOnTileMultipliers;
	
	public CombatTraits(JsonObject combatTraitsJson) {
		this.combatTraitsJson = combatTraitsJson;
		if(combatTraitsJson.containsKey("base_attack_strength")) {
			this.baseAttackStrength = combatTraitsJson.getJsonNumber("base_attack_strength").doubleValue();
			this.maximumRange = combatTraitsJson.getJsonNumber("attack_range").doubleValue();
			this.maximumElevationDelta = combatTraitsJson.getInt("max_elevation_delta");
		}else {
			this.baseAttackStrength = 0;
			this.maximumRange = 0;
			this.maximumElevationDelta = 0;
		}
		
		if (combatTraitsJson.containsKey("critical_hit_probability") && combatTraitsJson.containsKey("critical_hit_multiplier")){
			this.criticalHitProbability = combatTraitsJson.getJsonNumber("critical_hit_probability").doubleValue(); 
			this.criticalHitMultiplier = combatTraitsJson.getJsonNumber("critical_hit_multiplier").doubleValue(); 
		}else {
			this.criticalHitProbability = 0.0;
			this.criticalHitMultiplier = 0.0;
		}
		
		attackingAgainstUnitMultipliers = new CombatMultipliers<String>();
		attackingWithElevationDeltaMultipliers = new CombatMultipliers<String>();
		defendingOnTileMultipliers = new CombatMultipliers<String>();
		
		initializeAttackingAgainstUnitMultipliers();
		initializeAttackingWithElevationDeltaMultipliers();
		initializeDefendingOnTileMultipliers();
	}
	
	public boolean canAttackInEnvironment(BattleArea battleArea, Coordinate from, Coordinate to) {	
		if(this.maximumRange < from.getDistanceTo(to)) {
			return false;
		}
		
		if(Math.abs(battleArea.getElevationDelta(from, to)) > this.maximumElevationDelta) {
			return false;
		}
		
		return true;
	}
	
	public Set<Coordinate> getAttackableCoordinates(BattleArea battleArea, Coordinate location){
		Set<Coordinate> attackableCoordinates = new HashSet<Coordinate>();
		for (int row = 0; row < battleArea.getHeight(); row++) {
			for (int column = 0; column < battleArea.getWidth(); column++) {
				Coordinate c = new Coordinate(column, row);
				if (!c.equals(location) && this.canAttackInEnvironment(battleArea, location, c)) {
					attackableCoordinates.add(c);
				}
			}
		}
		
		return attackableCoordinates;
	}
	
	public double getAttackStrength(Unit defender, int elevationDelta) {
		double unitMultiplier = (1.0 + this.attackingAgainstUnitMultipliers.getPercentBonus(defender.getUnitClass()));
		
		String elevationDeltaType = getElevationDeltaType(elevationDelta);
		
		double elevationMultiplier = (1.0 + (this.attackingWithElevationDeltaMultipliers.getPercentBonus(elevationDeltaType)));
		
		return this.baseAttackStrength * unitMultiplier * elevationMultiplier;
	}
	
	public double getBaseAttackStrength() {
		return this.baseAttackStrength;
	}
	
	public double getCriticalHitProbability() {
		return this.criticalHitProbability;
	}
	
	public double getCriticalHitMultiplier() {
		return this.criticalHitMultiplier;
	}
	
	public double getDefenseMultiplier(BattleTile defenderTile) {
		return this.defendingOnTileMultipliers.getPercentBonus(defenderTile.getType());
	}
	
	public List<String> getReasonsForAttackStrength(Unit defender, int elevationDelta){
		List<String> reasons = new ArrayList<String>();
		reasons.add("Base Attack Strength: " + String.format("%.0f", this.baseAttackStrength));
		
		String elevationDeltaType = getElevationDeltaType(elevationDelta);
		
		double elevationBonus = this.attackingWithElevationDeltaMultipliers.getPercentBonus(elevationDeltaType);
		double unitBonus = this.attackingAgainstUnitMultipliers.getPercentBonus(defender.getUnitClass());
		
		if(unitBonus >= 0.0) {
			reasons.add("+"+String.format("%.0f", (unitBonus*100)) + "% from attacking " + defender.getUnitClass() + " units");
		}else if(unitBonus < 0.0){
			reasons.add(String.format("%.0f", (unitBonus*100)) + "% from attacking " + defender.getUnitClass() + " units");
		}
		
		String elevationExplanation = "";
		switch(elevationDeltaType) {
			case "uphill":
				elevationExplanation = "% from attacking uphill";
				break;
			case "downhill":
				elevationExplanation = "% from attacking downhill";
				break;
			case "level":
				elevationExplanation = "% from attacking on level ground";
				break;
		}
		
		if(elevationBonus >= 0.0) {
			reasons.add("+"+String.format("%.0f", (elevationBonus*100)) + elevationExplanation);
		}else if(elevationBonus < 0.0) {
			reasons.add(String.format("%.0f", (elevationBonus*100)) + elevationExplanation);
		}
		
		return reasons;
	}
	
	public List<String> getReasonsForDefenseMultiplier(BattleTile defenderTile){
		List<String> reasons = new ArrayList<String>();
		
		double tileBonus = getDefenseMultiplier(defenderTile);
		
		if(tileBonus > 0.0) {// less-than check reversed since the bonus is from the perspective of defender
			reasons.add("-" + String.format("%.0f", (tileBonus*100)) + "% from defender's tile (" + defenderTile.getType() + ")");
		}else if(tileBonus < 0.0){
			reasons.add("+"+String.format("%.0f", (tileBonus*-100)) + "% from defender's tile (" + defenderTile.getType() + ")");
		}else{
			reasons.add("+0% from defender's tile (" + defenderTile.getType() +")");
		}
		
		return reasons;
	}
	
	public JsonObject toJsonObject() {
		return combatTraitsJson;
	}

	private String getElevationDeltaType(int delta) {
		if (delta < 0) {
			return "uphill";
		}else if (delta > 0) {
			return "downhill";
		}else {
			return "level";
		}
	}

	private void initializeAttackingAgainstUnitMultipliers() {
		if (!combatTraitsJson.containsKey("attacking_against_unit_class_multipliers")) {
			return;
		}
		
		JsonObject unitMultipliers = combatTraitsJson.getJsonObject("attacking_against_unit_class_multipliers");
		for (String unitClass : Unit.UNIT_CLASSES) {
			if (unitMultipliers.containsKey(unitClass)) {
				this.attackingAgainstUnitMultipliers.setPercentBonus(unitClass, unitMultipliers.getJsonNumber(unitClass).doubleValue());
			}
		}
	}
	
	private void initializeAttackingWithElevationDeltaMultipliers() {
		if (!combatTraitsJson.containsKey("attacking_with_elevation_delta_multipliers")) {
			return;
		}
		
		JsonObject elevationMultipliers = combatTraitsJson.getJsonObject("attacking_with_elevation_delta_multipliers");
		for (String elevationDeltaType : ELEVATION_DELTA_TYPES) {
			if (elevationMultipliers.containsKey(elevationDeltaType)) {
				this.attackingWithElevationDeltaMultipliers.setPercentBonus(elevationDeltaType, elevationMultipliers.getJsonNumber(elevationDeltaType).doubleValue());
			}
		}
	}
	
	private void initializeDefendingOnTileMultipliers() {
		if (!combatTraitsJson.containsKey("defending_on_tile_multipliers")) {
			return;
		}
		
		JsonObject defendingOnTileMultipliers = combatTraitsJson.getJsonObject("defending_on_tile_multipliers");
		for (String battleTileType : BattleTile.BATTLE_TILE_TYPES) {
			if (defendingOnTileMultipliers.containsKey(battleTileType)) {
				this.defendingOnTileMultipliers.setPercentBonus(battleTileType, defendingOnTileMultipliers.getJsonNumber(battleTileType).doubleValue());
			}
		}
	}
	
}
