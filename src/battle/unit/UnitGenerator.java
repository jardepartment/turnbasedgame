package battle.unit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import util.ConfigFileHelper;

public class UnitGenerator {
	
	private static JsonArray parsedFactionsJsonArray; 
	
	static {
		try {
			File unitConfigFile = new File(ConfigFileHelper.getFactionConfigFile());
			InputStream unitConfigIs = new FileInputStream(unitConfigFile);
			JsonReader r = Json.createReader(unitConfigIs);
			parsedFactionsJsonArray = r.readArray();
			r.close();
			unitConfigIs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private UnitGenerator() {
		// Not constructable
	}

	public static Unit createNewUnit(String faction, String unitClass) {
		JsonObject unitJson = getUnitJsonObject(faction, unitClass);
		if (unitJson == null) {
			System.out.println("No unit matches criteria in " + ConfigFileHelper.getFactionConfigFile());
			return null;
		}
		
		return new Unit(unitJson.getString("name"), 
		                unitJson.getString("description"),
		                unitClass,
		                faction,
		                unitJson.getJsonNumber("max_health").doubleValue(),
		                new MovementTraits(unitJson.getJsonObject("movement_traits")),
		                new CombatTraits(unitJson.getJsonObject("combat_traits")));
	}

	private static JsonObject getFactionJsonObject(String faction) {
		if (!Unit.UNIT_FACTIONS.contains(faction)) {
			System.out.println("ERROR - No faction matching " + faction);
			return null;
		}
		
		for(int i = 0; i < parsedFactionsJsonArray.size(); i++) {
			JsonObject parsedFaction = parsedFactionsJsonArray.getJsonObject(i);
			
			if (faction.equalsIgnoreCase(parsedFaction.getString("faction"))) {
				return parsedFaction;
			}
		}
		
		System.out.println("ERROR - Faction doesn't exist in config file.");
		return null;
	}
	
	public static JsonArray getParsedFactionsJsonArray() {
		return parsedFactionsJsonArray;
	}

	
	private static JsonObject getUnitJsonObject(String faction, String unitClass) {
		JsonObject parsedFaction = getFactionJsonObject(faction);
		
		if (!Unit.UNIT_CLASSES.contains(unitClass)) {
			System.out.println("ERROR - No unit class matching " + unitClass);
			return null;
		}
		
		JsonArray parsedUnits = parsedFaction.getJsonArray("units");
		
		for(int i = 0; i < parsedUnits.size(); i++) {
			JsonObject unitJson = parsedUnits.getJsonObject(i);
			if (unitJson.getString("class").equalsIgnoreCase(unitClass)) {
				return unitJson;
			}
		}
		
		System.out.println("ERROR - unit doesn't exist in config file.");
		return null;
	}
}
