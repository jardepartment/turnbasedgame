package battle.unit;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;

import map.BattleArea;
import map.BattleTile;
import map.Coordinate;
import map.Move;

public class Unit {
	
	// TODO configure heal amount in factionconfig.json?
	// TODO configure heal range in factionconfig.json?
	public static final double LEADER_HEALING_AMOUNT = 25.0;
	public static final double LEADER_HEALING_RANGE = 2.5;
	
	public final static Set<String> UNIT_FACTIONS = new HashSet<String>(Arrays.asList("desert", "forest", "mountain"));
	public final static Set<String> UNIT_CLASSES = new HashSet<String>(Arrays.asList("melee", "ranged", "mounted", "leader"));
	
	private static HashSet<Integer> unitIds = new HashSet<Integer>();
	private static Random rng = new Random();
	
	private String name;
	private String description;
	private String unitClass;
	private String unitFaction;
	private double maxHealth;
	private double health;
	private Coordinate location;
	private CombatTraits combatTraits;
	private MovementTraits movementTraits;
	private int uuid;
	private boolean hasMoved;
	private boolean hasPerformedAction;
	
	public Unit(String name,
	            String description,
	            String unitClass,
	            String unitFaction,
	            double maxHealth,
	            MovementTraits movementTraits,
	            CombatTraits combatTraits) {
		setUuid();
		this.name = name;
		this.description = description;
		this.unitClass = unitClass;
		this.unitFaction = unitFaction;
		this.maxHealth = maxHealth;
		
		// set to invalid coordinates
		this.location = new Coordinate(-1,-1);
		this.health = maxHealth;
		this.movementTraits = movementTraits;
		this.combatTraits = combatTraits;
		this.hasMoved = false;
		this.hasPerformedAction = false;
	}
	
	public boolean canHeal() {
		return this.unitClass.equalsIgnoreCase("leader");
	}

	public Set<Coordinate> getAttackableTiles(BattleArea battleArea){
		return this.combatTraits.getAttackableCoordinates(battleArea, this.location);
	}

	public double getAttackStrength(Unit defender, int elevationDelta) {
		return this.combatTraits.getAttackStrength(defender, elevationDelta);
	}

	public CombatTraits getCombatTraits() {
		return this.combatTraits;
	}

	public double getDefenseMultiplier(BattleTile defenderTile) {
		return this.combatTraits.getDefenseMultiplier(defenderTile);
	}

	public String getDescription() {
		return this.description;
	}
	
	public Set<Coordinate> getHealableTiles(BattleArea battleArea){
		Set<Coordinate> healableCoordinates = new HashSet<Coordinate>();
		if (!canHeal()) {
			return healableCoordinates;
		}
		
		for(int y = 0; y < battleArea.getHeight(); y++) {
			for(int x = 0; x < battleArea.getWidth(); x++) {
				Coordinate c = new Coordinate(x, y);
				if (c.equals(this.location)) {
					continue;
				}
				
				if(this.location.getDistanceTo(c) <= LEADER_HEALING_RANGE) {
					healableCoordinates.add(c);
				}
			}
		}
		
		return healableCoordinates;
	}

	public double getHealth(){
		return this.health;
	}

	public boolean getIsAlive() {
		return (this.health > 0.0);
	}

	public Coordinate getLocation(){
		return this.location;
	}

	public double getMaximumHealth() {
		return this.maxHealth;
	}

	public double getMaxMovementRange() {
		return this.movementTraits.getMaximumRange();
	}

	public String getName() {
		return this.name;
	}

	public Collection<Move> getReachableMoves(BattleArea battleArea, Set<Coordinate> enemyCoordinates){
		return this.movementTraits.getPossibleMoves(battleArea, this.location, enemyCoordinates);
	}

	public String getUnitClass() {
		return this.unitClass;
	}

	public int getUuid() {
		return this.uuid;
	}

	public boolean hasMoved() {
		return this.hasMoved;
	}
	
	public boolean hasPerformedAction() {
		return this.hasPerformedAction;
	}

	public void move(Coordinate location) {
		this.hasMoved = true;
		this.setLocation(location);
	}

	public void performAction() {
		this.hasPerformedAction = true;
	}

	public void resetHasMovedAndAttacked() {
		this.hasPerformedAction = false;
		this.hasMoved = false;
	}

	public void setHasMoved() {
		this.hasMoved = true;
	}
	
	public void setHealth(double health){
		if(health < 0.0) {
			this.health = 0.0;
		}else {
			this.health = health;
		}
	}
	
	public JsonObject toJsonObject() {
		return Json.createObjectBuilder()
				.add("id", this.uuid)
				.add("name", this.name)
				.add("description", this.description)
				.add("unit_class", this.unitClass)
				.add("unit_faction", this.unitFaction)
				.add("is_alive", this.getIsAlive())
				.add("x", this.location.x)
				.add("y", this.location.y)
				.add("current_health", (int)(this.health))
				.add("maximum_health", this.maxHealth)
				.add("has_moved", this.hasMoved)
				.add("has_performed_action", this.hasPerformedAction)
				.add("movement_traits", this.movementTraits.toJsonObject())
				.add("combat_traits", this.combatTraits.toJsonObject())
				.add("graphical_coord", new Coordinate().toJsonObject())// invalid coordinate is fine, the front end is responsible for calculating this
				.build();
	}
	
	private void setLocation(Coordinate location){
		this.location = location;
	}

	private void setUuid() {
		// pulled 200k OOMA
		int id = rng.nextInt(200000);
		while(unitIds.contains(id)) {
			id = rng.nextInt(200000);
		}
		
		this.uuid = id;
		unitIds.add(id);
	}
}
