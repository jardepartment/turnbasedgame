package battle.unit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.JsonObject;

import map.BattleArea;
import map.Coordinate;
import map.Move;

public class MovementTraits {
	
	private JsonObject movementTraitsJson;
	private double maximumRange;
	private int maximumElevationDelta;
	
	public MovementTraits(JsonObject movementTraitsJson) {
		this.movementTraitsJson = movementTraitsJson;
		this.maximumRange = movementTraitsJson.getJsonNumber("max_movement_range").doubleValue();
		this.maximumElevationDelta = movementTraitsJson.getInt("max_elevation_delta");
	}
	
	public int getMaximumElevationDelta() {
		return this.maximumElevationDelta;
	}

	public double getMaximumRange() {
		return this.maximumRange;
	}
	
	public Set<Move> getNextMoves(BattleArea battleArea, Move currentMove, double rangeLeft, Set<Coordinate> impassableCoords){
		Set<Move> nextMoves = new HashSet<Move>();
		Coordinate start = currentMove.getFinalLocation();
		Set<Coordinate> adjacentCoords = battleArea.getAdjacentCoordinates(start);
		for (Coordinate c : adjacentCoords) {
			if((impassableCoords != null) && (impassableCoords.contains(c))) {
				continue;
			}
			
			double distance = start.getDistanceTo(c);
			if ((distance <= rangeLeft) &&
				(Math.abs(battleArea.getElevationDelta(start, c)) <= this.maximumElevationDelta)) {
				List<Coordinate> newSteps = new ArrayList<Coordinate>(currentMove.getSteps());
				newSteps.add(c);
				nextMoves.add(new Move((currentMove.getDistance() + distance), c, newSteps));
			}
		}
		
		return nextMoves;
	}

	public Collection<Move> getPossibleMoves(BattleArea battleArea, Coordinate startingCoord, Set<Coordinate> impassableCoords) {
		Set<Move> uncheckedMoves = new HashSet<Move>();
		Map<Coordinate, Move> possibleMoves = new HashMap<Coordinate, Move>();
		Move initialMove = new Move(0.0, startingCoord, new ArrayList<Coordinate>());
		uncheckedMoves.add(initialMove);
		possibleMoves.put(startingCoord, initialMove);
		
		while (!uncheckedMoves.isEmpty()) {
			Move current = (Move)uncheckedMoves.toArray()[0];
			Coordinate c = current.getFinalLocation();
			
			if (!possibleMoves.keySet().contains(c)){
				possibleMoves.put(c, current);
			} else if (current.isEqualAndBetterThan(possibleMoves.get(c))) {
				possibleMoves.remove(c);
				possibleMoves.put(c, current);
			} 
			
			Set<Move> nextMoves = getNextMoves(battleArea, current, (this.maximumRange-current.getDistance()), impassableCoords);
			for (Move next : nextMoves) {
				uncheckedMoves.add(next);
			}
			
			uncheckedMoves.remove(current);
		}
		
		possibleMoves.remove(startingCoord);
		
		return possibleMoves.values();
	}

	public JsonObject toJsonObject() {
		return movementTraitsJson;
	}
}
