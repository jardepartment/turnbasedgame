package battle;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import battle.unit.Unit;

import map.Coordinate;

public class Army {
	
	private Map<Integer, Unit> units;
	private Unit selectedUnit;
	private boolean isReady;
	private Integer leaderUnitUuid;
	
	public Army(){
		units = new HashMap<Integer, Unit>();
		isReady = false;
	}
	
	public boolean addUnit(Unit u){
		Integer uuid = u.getUuid();
		if(this.units.containsKey(uuid)) {
			System.out.println("Unit with id " + u.getUuid() + " already exists in army");
			return false;
		}
		
		this.units.put(uuid, u);
		
		if(u.getUnitClass().equals("leader")) {
			this.leaderUnitUuid = uuid;
		}
		
		return true;
	}
	
	public void deleteAllUnits() {
		this.units.clear();
	}
	
	public Set<Coordinate> getAliveUnitCoordinates(){
		Set<Coordinate> unitCoordinates = new HashSet<Coordinate>();
		for (Unit u : units.values()) {
			if(u.getIsAlive()) {
				unitCoordinates.add(u.getLocation());
			}
		}
		
		return unitCoordinates;
	}
	
	public Unit getLeaderUnit() {
		return this.units.get(leaderUnitUuid);
	}
	
	public int getNumberOfActionsUsed() {
		int actionsUsed = 0;
		for (Unit u : units.values()) {
			if (u.hasPerformedAction()) {
				actionsUsed++;
			}
		}
		
		return actionsUsed;
	}
	
	public int getNumberOfMovesUsed() {
		int unitsMoved = 0;
		for (Unit u : units.values()) {
			if (u.hasMoved()) {
				unitsMoved++;
			}
		}
		
		return unitsMoved;
	}
	
	public Unit getSelectedUnit() {
		return this.selectedUnit;
	}
	
	public double getTotalUnitAttackStrength() {
		double totalAttackStrength = 0.0;
		for (Unit u : units.values()) {
			if(u.getIsAlive()) {
				totalAttackStrength += u.getCombatTraits().getBaseAttackStrength();
			}
		}
		
		return totalAttackStrength;
	}
	
	public double getTotalUnitHealth() {
		double totalHealth = 0.0;
		for (Unit u : units.values()) {
			if(u.getIsAlive()) {
				totalHealth += u.getHealth();
			}
		}
		
		return totalHealth;
	}
	
	public Unit getUnit(int unitId) {
		return units.get(unitId);
	}
	
	public Unit getUnitAtLocation(Coordinate coordinate) {
		for(Unit u : units.values()) {
			if(u.getLocation().equals(coordinate) && u.getIsAlive()) {
				return u;
			}
		}
		
		return null;
	}
	
	public Collection<Unit> getUnits(){
		return this.units.values();
	}
	
	public JsonArray getUnitsOnMapAsJsonArray() {
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for(int id : units.keySet()) {
			// valid coordinate means on map/not in inventory
			if (units.get(id).getLocation().x >= 0) {
				arrayBuilder.add(Json.createObjectBuilder(units.get(id).toJsonObject()).add("id", id));
			}
		}
		
		return arrayBuilder.build();
	}
	
	public boolean hasLost() {
		for(Unit u : units.values()) {
			if(u.getIsAlive()) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean hasMovedXTimes(int x) {
		int unitsMoved = 0;
		for (Unit u : units.values()) {
			if (u.hasMoved()) {
				unitsMoved++;
			}
		}
		
		return (unitsMoved >= x);
	}
	
	public boolean hasUsedXActions(int x) {
		int actionsUsed = 0;
		for (Unit u : units.values()) {
			if (u.hasPerformedAction()) {
				actionsUsed++;
			}
		}
		
		return (actionsUsed >= x);
	}
	
	public boolean isReady() {
		return this.isReady;
	}

	public void resetUnitMovesAndAttacks() {
		for (Unit u : units.values()) {
			u.resetHasMovedAndAttacked();
		}
	}
	
	public void selectUnit(int unitId) {
		this.selectedUnit = units.get(unitId);
	}
	
	public void setIsReady(boolean isReady) {
		this.isReady = isReady;
	}
}
