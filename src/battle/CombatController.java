
package battle;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import battle.unit.Unit;
import map.BattleArea;
import map.BattleTile;
import map.Coordinate;

public class CombatController {
	
	public static JsonObject attack(Army attackingArmy, JsonObject jsonObject, Unit attacker, Battle battle) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		BattleArea battleArea = battle.getBattleArea();
		Coordinate defenderCoordinate = new Coordinate(jsonObject.getInt("x"), jsonObject.getInt("y"));
		
		if(attackingArmy.getUnitAtLocation(defenderCoordinate) != null) {
			builder.add("success", false);
			builder.add("message", "can't attack own unit");
			return builder.build();
		}
		
		Unit defender = battle.getUnitAtLocation(defenderCoordinate);
		if(defender == null) {
			builder.add("success", false);
			builder.add("message", "no unit at location");
			return builder.build();
		}
		
		builder.add("attacker", attacker.toJsonObject());
		builder.add("defender", defender.toJsonObject());
		
		if(!attacker.getCombatTraits().canAttackInEnvironment(battleArea, attacker.getLocation(), defenderCoordinate)) {
			builder.add("success", false);
			builder.add("message", "Unit is too far away to be attacked");
			
			return builder.build();
		}
		
		BattleTile defenderTile = battleArea.getTile(defenderCoordinate);
		BattleTile attackerTile = battleArea.getTile(attacker.getLocation());
		int elevationDelta = battleArea.getElevationDelta(attackerTile, defenderTile);
		double attackerStrength = attacker.getAttackStrength(defender, elevationDelta);
		double defenderMultiplier = defender.getDefenseMultiplier(defenderTile);
		
		boolean isCriticalHit = false;
		Double critcalHitRoll = Math.random();
		
		if (critcalHitRoll < attacker.getCombatTraits().getCriticalHitProbability()) {
			isCriticalHit = true;
			attackerStrength = attackerStrength * (1.0 + attacker.getCombatTraits().getCriticalHitMultiplier());
			System.out.println("CRITICAL HIT! attack strength increased to " + attackerStrength);
		}
		
		
		attacker.performAction();
		double damageOutput = calculateDamageOutput(attackerStrength, defenderMultiplier);
		defender.setHealth(defender.getHealth()-damageOutput);
		
		builder.add("success", true);
		builder.add("is_critical_hit", isCriticalHit);
		builder.add("unit_killed", !defender.getIsAlive());
		builder.add("damage_dealt", Integer.toString((int)(Math.round(damageOutput))));
		return builder.build();
	}
	
	public static JsonArray getPossibleAttacksJson(Battle battle, Unit unit, Army defendingArmy) {
		Coordinate unitLocation = unit.getLocation();
		JsonArrayBuilder possibleAttacksBuilder = Json.createArrayBuilder();
		for(Unit enemyUnit : defendingArmy.getUnits()) {
			Coordinate enemyLocation = enemyUnit.getLocation();
			if (enemyLocation.x < 0) {
				continue;
			}
			
			double simulatedResults = getSimulatedResults(battle.getBattleArea(), unit, enemyUnit);
			if(simulatedResults != 0.0) {
				JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
				objectBuilder.add("defending_unit_name", enemyUnit.getName());
				objectBuilder.add("defending_unit_current_health", (int)enemyUnit.getHealth());
				objectBuilder.add("expected_damage_output", String.format("%.0f", simulatedResults));
				objectBuilder.add("critical_hit_probability", (int)(100*unit.getCombatTraits().getCriticalHitProbability()) + "%");
				objectBuilder.add("critical_hit_multiplier", "+" + (int)(100*unit.getCombatTraits().getCriticalHitMultiplier()) + "%");
				objectBuilder.add("x", unitLocation.x);
				objectBuilder.add("y", unitLocation.y);
				objectBuilder.add("enemy_x", enemyLocation.x);
				objectBuilder.add("enemy_y", enemyLocation.y);
				JsonArrayBuilder attackReasonArrayBuilder = Json.createArrayBuilder();
				List<String> attackStrengthReasons = unit.getCombatTraits().getReasonsForAttackStrength(enemyUnit, 
													     battle.getBattleArea().getElevationDelta(unitLocation, enemyLocation));
				for(String s : attackStrengthReasons) {
					attackReasonArrayBuilder.add(s);
				}
				
				objectBuilder.add("attack_strength_reasons", attackReasonArrayBuilder.build());
				
				JsonArrayBuilder defenseReasonArrayBuilder = Json.createArrayBuilder();
				List<String> defenseMultiplierReasons = enemyUnit.getCombatTraits().getReasonsForDefenseMultiplier(battle.getBattleArea().getTile(enemyLocation));
				for(String s : defenseMultiplierReasons) {
					defenseReasonArrayBuilder.add(s);
				}
				
				objectBuilder.add("defense_multiplier_reasons", defenseReasonArrayBuilder.build());
				
				possibleAttacksBuilder.add(objectBuilder.build());
			}
		}
		
		return possibleAttacksBuilder.build();
	}
	
	public static JsonArray getPossibleHealsJson(Battle battle, Unit unit, Army ownArmy) {
		JsonArrayBuilder possibleHealsBuilder = Json.createArrayBuilder();
		Coordinate location = unit.getLocation();
		for(Unit ownUnit : ownArmy.getUnits()) {
			Coordinate friendlyLocation = ownUnit.getLocation();
			
			if (!canHealInEnvironment(unit, ownUnit)) {
				continue;
			}

			double healOutput = calculateHealOutput(Unit.LEADER_HEALING_AMOUNT, ownUnit);
			
			if(healOutput != 0.0) {
				JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
				objectBuilder.add("unit_to_heal_name", ownUnit.getName());
				objectBuilder.add("unit_to_heal_current_health", (int)ownUnit.getHealth());
				objectBuilder.add("unit_to_heal_max_health", (int)ownUnit.getMaximumHealth());
				objectBuilder.add("expected_heal_amount", String.format("%.0f", healOutput));
				objectBuilder.add("x", location.x);
				objectBuilder.add("y", location.y);
				objectBuilder.add("friendly_x", friendlyLocation.x);
				objectBuilder.add("friendly_y", friendlyLocation.y);
				
				possibleHealsBuilder.add(objectBuilder.build());
			}
		}
		
		return possibleHealsBuilder.build();
	}
	
	public static JsonObject heal(Army healingArmy, JsonObject jsonObject, Unit healer, Battle battle) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		Coordinate unitToHealCoordinate = new Coordinate(jsonObject.getInt("x"), jsonObject.getInt("y"));
		
		Unit unitToHeal = battle.getUnitAtLocation(unitToHealCoordinate);
		if(unitToHeal == null) {
			builder.add("success", false);
			builder.add("message", "no unit at location");
			return builder.build();
		}
		
		if(healingArmy.getUnitAtLocation(unitToHealCoordinate) == null) {
			builder.add("success", false);
			builder.add("message", "can't heal enemy units");
			return builder.build();
		}
		
		if(unitToHeal.getHealth() == unitToHeal.getMaximumHealth()) {
			builder.add("success", false);
			builder.add("message", "unit health is full");
			return builder.build();
		}
		
		builder.add("healer", healer.toJsonObject());
		builder.add("unit_to_heal", unitToHeal.toJsonObject());
		
		if(!canHealInEnvironment(healer, unitToHeal)) {
			builder.add("success", false);
			builder.add("message", "Unit is too far away to be healed");
			
			return builder.build();
		}
		
		double healOutput = calculateHealOutput(Unit.LEADER_HEALING_AMOUNT, unitToHeal);
		healer.performAction(); // heals count as an attack action
		unitToHeal.setHealth(unitToHeal.getHealth()+healOutput);
		
		builder.add("success", true);
		builder.add("healing_dealt", Integer.toString((int)(Math.round(healOutput))));
		return builder.build();
	}
	
	private static double calculateDamageOutput(double attackStrength, double defenderMultiplier) {
		if (defenderMultiplier > 1.0) {
			System.out.println("Error- defender multiplier (" + defenderMultiplier +  ") makes the attack strength negative. Ignoring multiplier");
			return attackStrength;
		}

		return attackStrength * (1.0 - defenderMultiplier);
	}
	
	private static double calculateHealOutput(double healAmount, Unit unitToHeal) {
		double amountFromFullHealth = unitToHeal.getMaximumHealth() - unitToHeal.getHealth();
		if (amountFromFullHealth < healAmount) {
			return amountFromFullHealth;
		}else {
			return healAmount;
		}
	}
	
	private static boolean canHealInEnvironment(Unit healer, Unit healee) {
		if (!healer.canHeal()) {
			return false;
		}
		
		if(healer.getLocation().getDistanceTo(healee.getLocation()) > Unit.LEADER_HEALING_RANGE) {
			return false;
		}
		
		if (!healer.getIsAlive() || !healee.getIsAlive()) {
			return false;
		}
		
		if(healer.getUuid() == healee.getUuid()) {
			return false;
		}
		
		return true;
	}

	private static double getSimulatedResults(BattleArea battleArea, Unit attacker, Unit defender) {
		if(!attacker.getCombatTraits().canAttackInEnvironment(battleArea, attacker.getLocation(), defender.getLocation())) {
			return 0.0;
		}
		
		if(!defender.getIsAlive()) {
			return 0.0;
		}
		
		BattleTile defenderTile = battleArea.getTile(defender.getLocation());
		BattleTile attackerTile = battleArea.getTile(attacker.getLocation());
		int elevationDelta = battleArea.getElevationDelta(attackerTile, defenderTile);
		double attackerStrength = attacker.getAttackStrength(defender, elevationDelta);
		double defenderMultiplier = defender.getDefenseMultiplier(defenderTile); 
		return calculateDamageOutput(attackerStrength, defenderMultiplier);
	}
}
