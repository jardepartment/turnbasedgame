package battle;

import java.util.Collection;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.Session;

import battle.unit.Unit;
import battle.unit.UnitGenerator;
import server.ServerSocket;
import server.User;
import map.BattleArea;
import map.BattleTile;
import map.Coordinate;
import map.Move;

public class Battle {
	
	private enum BattleState{
		SETUP,
		IN_PROGRESS,
		END
	}
	
	private static int MAX_MOVES_PER_TURN = 2;
	private static int MAX_ATTACKS_PER_TURN = 2;
	
	private BattleState battleState;
	private BattleArea battleArea;
	
	private User leftUser;
	private User rightUser;
	
	private User activeUser = null;
	private User waitingUser = null;
	
	public Battle(JsonObject mapJson, User leftUser, User rightUser) {
		
		this.leftUser = leftUser;
		this.rightUser = rightUser;
		
		String leftFaction = leftUser.getFaction();
		String rightFaction = rightUser.getFaction();
		leftUser.resetArmy();
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "melee"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "melee"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "ranged"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "ranged"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "mounted"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "mounted"));
		leftUser.getArmy().addUnit(UnitGenerator.createNewUnit(leftFaction, "leader"));

		rightUser.resetArmy();
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "melee"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "melee"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "ranged"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "ranged"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "mounted"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "mounted"));
		rightUser.getArmy().addUnit(UnitGenerator.createNewUnit(rightFaction, "leader"));
		
	    battleArea = new BattleArea(mapJson);
	    battleArea.printGrid();
	    int spawnSize = battleArea.getHeight()*BattleArea.NUMBER_OF_SPAWN_COLUMNS;
	    if ((spawnSize <= leftUser.getArmy().getUnits().size()) ||
	    	(spawnSize <= rightUser.getArmy().getUnits().size()))  {
	    	throw new RuntimeException("spawn size(" + spawnSize + ") is too small for one of the armies");
	    }
	}

	public BattleArea getBattleArea() { 
		return this.battleArea;
	}
	
	// checks both armies' units
	public Unit getUnitAtLocation(Coordinate c) {
		Unit unit = leftUser.getArmy().getUnitAtLocation(c);
		if(unit != null) {
			return unit;
		}

		return rightUser.getArmy().getUnitAtLocation(c);
	}
	
	public void handleMessage(JsonObject message, Session session) {
		switch(message.getString("message_id")) {
			case "player_ready":
				System.out.println("player ready received from session " + session.getId());
				getUserBySession(session).getArmy().setIsReady(true);
				if(leftUser.getArmy().isReady() && rightUser.getArmy().isReady()) {
					startBattle();
				}
				break;
			case "player_left_battle":
				handleForfeit(session);
				this.battleState = BattleState.END;
				break;
			case "game_over":
				this.battleState = BattleState.END;
				break;
			case "inventory_select":
				System.out.println("inventory select received from session " + session.getId());
				getUserBySession(session).getArmy().selectUnit(message.getInt("unit_id"));
				break;
			case "end_turn":
				System.out.println("end turn received from session " + session.getId());
				handleTurnChange();
				break;
			case "tile_select":
				System.out.println("tile select received from session " + session.getId());
				handleSelect(message.getInt("x"), message.getInt("y"), session);
				break;
			case "unit_move":
				System.out.println("unit move received from session " + session.getId());
				handleMove(message.getInt("x"), message.getInt("y"), session);
				break;
			case "attack":
				System.out.println("attack received from session " + session.getId());
				handleAttack(session, message);
				break;
			case "new_chat_message":
				System.out.println("Chat received from " + session.getId());
				handleChat(message.getString("chat_contents"), session);
				break;
			default:
				System.out.println("Battle - unknown message received");
		}
	}
	
	public void initializeMapAndUnits(int battleId) {
		this.battleState = BattleState.SETUP;
		
		JsonObject mapInitMessage = Json.createObjectBuilder(battleArea.getMapAsJson()).add("message_id", "battle_map_init").add("battle_id", battleId).build();
		System.out.println("sending map");
		ServerSocket.sendJsonObject(leftUser.getSession(), mapInitMessage);
		ServerSocket.sendJsonObject(rightUser.getSession(), mapInitMessage);
		
		updateBattleOverview();
		
		sendInitialUnitMessage(leftUser, true);
		sendInitialUnitMessage(rightUser, false);
	}
	
	private JsonArray getAttackableTiles(Unit u) {
		Collection<Coordinate> attackableTiles = u.getAttackableTiles(this.battleArea);
/*		if(attackableTiles == null) {
			return null;
		}*/
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for(Coordinate c : attackableTiles) {
			arrayBuilder.add(c.toJsonObject());
		}
		
		return arrayBuilder.build();
	}
	
	private JsonArray getHealableTiles(Unit u) {
		Collection<Coordinate> healableTiles = u.getHealableTiles(this.battleArea);
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for(Coordinate c : healableTiles) {
			arrayBuilder.add(c.toJsonObject());
		}
		
		return arrayBuilder.build();
	}
	
	private Move getMove(Unit u, Coordinate moveToCoordinate){
		if (u == null) {
			return null;
		}
		
		Collection<Move> possibleMoves = u.getReachableMoves(this.battleArea, waitingUser.getArmy().getAliveUnitCoordinates());
		for (Move m : possibleMoves) {
			Coordinate finalLocation = m.getFinalLocation();
			if ((getUnitAtLocation(finalLocation) == null) && (finalLocation.equals(moveToCoordinate))){
				return m;
			}
		}
		
		return null; 
	}
	
	private JsonObject getOverallChances() {
		
		double leftUnitHealth = leftUser.getArmy().getTotalUnitHealth();
		double leftUnitStrength = leftUser.getArmy().getTotalUnitAttackStrength();
		
		double rightUnitHealth = rightUser.getArmy().getTotalUnitHealth();
		double rightUnitStrength = rightUser.getArmy().getTotalUnitAttackStrength();
		
		double leftUnitHealthPercentage = leftUnitHealth / (leftUnitHealth + rightUnitHealth);
		double leftUnitStrengthPercentage = leftUnitStrength / (leftUnitStrength + rightUnitStrength);
				
		double leftUnitOverallPercentage = (leftUnitHealthPercentage + leftUnitStrengthPercentage) / 2.0;
		
		return Json.createObjectBuilder()
				.add("left_unit_health", String.format("%.2f", leftUnitHealth))
				.add("left_unit_strength", String.format("%.2f", leftUnitStrength))
				.add("right_unit_health", String.format("%.2f", rightUnitHealth))
				.add("right_unit_strength", String.format("%.2f", rightUnitStrength))
				.add("left_percentage", leftUnitOverallPercentage)
				.build();
	}
	
	private JsonArray getPossibleMovesJson(Unit u, Session session) {
		// includes invalid moves (moves to an already occupied tile)
		Set<Coordinate> enemyCoordinates;
		
		if(waitingUser != null) {
			if (waitingUser.getSession().equals(session)) {
				if (null == waitingUser.getArmy().getUnit(u.getUuid())) {
					enemyCoordinates = waitingUser.getArmy().getAliveUnitCoordinates();
				}else {
					enemyCoordinates = activeUser.getArmy().getAliveUnitCoordinates();
				}
			} else {
				if (null == activeUser.getArmy().getUnit(u.getUuid())) {
					enemyCoordinates = activeUser.getArmy().getAliveUnitCoordinates();
				}else {
					enemyCoordinates = waitingUser.getArmy().getAliveUnitCoordinates();
				}
			}
		}else {
			enemyCoordinates = null;
		}
		
		Collection<Move> reachableMoves = u.getReachableMoves(this.battleArea, enemyCoordinates);
		if(reachableMoves == null) {
			return null;
		}
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for(Move m : reachableMoves) {
			arrayBuilder.add(m.toJsonObject());
		}
		
		return arrayBuilder.build();
	}
	
	private JsonObjectBuilder getTileInfo(Coordinate coordinate) {
		BattleTile tile = battleArea.getTile(coordinate);
		return Json.createObjectBuilder()
				.add("type", tile.getType())
				.add("elev", tile.getElevation());
	}
	
	private JsonObjectBuilder getUnitInfo(Coordinate coordinate, Session session) {
		
		Unit unit = getUnitAtLocation(coordinate);
		if(unit == null) {
			return null;
		}
		
		User user = getUserBySession(session);
		if(user == null) {
			return null;
		}
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("unit", unit.toJsonObject());
		builder.add("possible_moves", getPossibleMovesJson(unit, session));
		builder.add("attackable_tiles", getAttackableTiles(unit));
		builder.add("healable_tiles", getHealableTiles(unit));
		
		// If user does not 'own' the unit clicked on
		if(user.getArmy().getUnit(unit.getUuid()) == null) {
			if (this.battleState == BattleState.SETUP) {
				return null;
			} else {
				return builder;
			}
		}
		
		user.getArmy().selectUnit(unit.getUuid());
		JsonArray possibleAttacks = null;
		JsonArray possibleHeals = null;
		if(!unit.hasPerformedAction() && (this.battleState == BattleState.IN_PROGRESS)) {
			if(user.equals(leftUser)) {
				possibleAttacks = CombatController.getPossibleAttacksJson(this, unit, rightUser.getArmy());
				possibleHeals = CombatController.getPossibleHealsJson(this, unit, leftUser.getArmy());
			}else if(user.equals(rightUser)) {
				possibleAttacks = CombatController.getPossibleAttacksJson(this, unit, leftUser.getArmy());
				possibleHeals = CombatController.getPossibleHealsJson(this, unit, rightUser.getArmy());
			}else{
				System.out.println("user did not match left or right user");
				return null;
			}
		}
		
		if(possibleAttacks != null) {
			builder.add("possible_attacks", possibleAttacks);
		}
		
		if(possibleHeals != null) {
			builder.add("possible_heals",  possibleHeals);
		}
		
		return builder;
	}
	
	private User getUserBySession(Session session) {
		if(session.getId().equals(leftUser.getSession().getId())) {
			return leftUser;
		}else if(session.getId().equals(rightUser.getSession().getId())) {
			return rightUser;
		}
		
		return null;
	}
	
	private void handleAttack(Session session, JsonObject attackJson){
		if(this.battleState != BattleState.IN_PROGRESS) {
			System.out.println("wasn't expecting an attack message in battle state " + this.battleState);
			return;
		}
		if(!session.getId().equals(activeUser.getSession().getId())) {
			System.out.println("wasn't expecting an attack message since it is not " + session.getId() + "'s turn.");
			return;
		}
		
		Army activeArmy = activeUser.getArmy();
		Unit attacker = activeArmy.getUnit(attackJson.getInt("selected_unit_id"));
		if(attacker == null) {
			System.out.println("Can't attack with enemy's unit");
			return;
		}
		
		if (attacker.hasPerformedAction()) {
			System.out.println("Unit has already performed its action!!");
			return;
		}
		
		if (activeArmy.hasUsedXActions(MAX_ATTACKS_PER_TURN)) {
			System.out.println("Units already attacked " + MAX_ATTACKS_PER_TURN + " times");
			return;
		}
		
		if (attacker.getUnitClass().equals("leader")) {
			JsonObject healResults = CombatController.heal(activeArmy, attackJson, attacker, this);
			JsonObject healMessageJson = Json.createObjectBuilder()
					.add("message_id", "heal_results")
					.add("results", healResults)
					.add("healer_username", activeUser.getUsername()).build();	
					
			ServerSocket.sendJsonObject(leftUser.getSession(), healMessageJson);
			ServerSocket.sendJsonObject(rightUser.getSession(), healMessageJson);
		}else {
			JsonObject attackResults = CombatController.attack(activeArmy, attackJson, attacker, this);
			JsonObject attackMessageJson = Json.createObjectBuilder()
					.add("message_id", "attack_results")
					.add("results", attackResults)
					.add("attacker_username", activeUser.getUsername())
					.add("attacked_username", waitingUser.getUsername()).build();		;
					
			ServerSocket.sendJsonObject(leftUser.getSession(), attackMessageJson);
			ServerSocket.sendJsonObject(rightUser.getSession(), attackMessageJson);
		}

		updateBattleOverview();
		updateAllUnits();
	}
	
	private void handleForfeit(Session forfeitedPlayerSession) {
		if (leftUser.getSession().equals(forfeitedPlayerSession)){
			sendBattleOverMessage(rightUser, leftUser);
		} else if (rightUser.getSession().equals(forfeitedPlayerSession)) {
			sendBattleOverMessage(leftUser, rightUser);
		} else {
			throw new RuntimeException("No user found with session ID " + forfeitedPlayerSession.getId());
		}
	}
	
	private void handleMove(int x, int y, Session session) {
		Coordinate moveToCoordinate = new Coordinate(x, y);
		if(this.battleState == BattleState.SETUP) {
			User user = getUserBySession(session);
			Army army = user.getArmy();
			boolean isLeftSide = session.getId().equals(leftUser.getSession().getId());
			Set<Coordinate> possibleSpawnCoordinates = battleArea.getPossibleSpawnCoordinates(isLeftSide);
			if((!possibleSpawnCoordinates.contains(moveToCoordinate) || this.getUnitAtLocation(new Coordinate(x, y)) != null)) {
				ServerSocket.sendJsonObject(session, Json.createObjectBuilder().add("message_id", "movement_error").build());
				return;
			}
				
			army.getSelectedUnit().move(new Coordinate(x, y));
			updateOwnUnits(user);
		}else if(this.battleState == BattleState.IN_PROGRESS){
			if(!session.getId().equals(activeUser.getSession().getId())) {
				ServerSocket.sendJsonObject(session, Json.createObjectBuilder().add("message_id", "movement_error").build());
				return;
			}
			
			Army army = activeUser.getArmy();
			if (army.hasMovedXTimes(MAX_MOVES_PER_TURN)) {
				ServerSocket.sendJsonObject(session, Json.createObjectBuilder().add("message_id", "movement_error").build());
				return;
			}
			
			Unit selectedUnit = army.getSelectedUnit();
			
			Move move = getMove(selectedUnit, moveToCoordinate);
			if(move == null || selectedUnit.hasMoved())
			{
				ServerSocket.sendJsonObject(session, Json.createObjectBuilder().add("message_id", "movement_error").build());
				return;
			}
		
			selectedUnit.setHasMoved();
			
			// ordering here matters. updateAllUnits after the setHasMoved has occured, 
			// but let the front end do the actual movement display ie physical x,y is the
			// pre-moved version during this updateAllUnits call
			updateAllUnits();
	
			sendMove(selectedUnit, move);
			selectedUnit.move(moveToCoordinate);
			
			// check for victory move
			if((moveToCoordinate.equals(battleArea.getObjectiveCoordinate()) && selectedUnit.getUnitClass().equals("leader"))) {
				
				User winner;
				User loser;
				Unit rightLeaderUnit = rightUser.getArmy().getLeaderUnit();
				if ((rightLeaderUnit != null) && rightLeaderUnit.getLocation().equals(battleArea.getObjectiveCoordinate())) {
					winner = rightUser;
					loser = leftUser;
				}else{// if((leftLeaderUnit != null) && leftLeaderUnit.getLocation().equals(battleArea.getObjectiveCoordinate())) {
					winner = leftUser;
					loser = rightUser;
				}
				
				sendBattleOverMessage(winner, loser);
			}
		}
	}

	private void handleSelect(int x, int y, Session session) {
		Coordinate selectedCoordinate = new Coordinate(x, y);
		if(!selectedCoordinate.isValid(new Coordinate((battleArea.getWidth()-1), (battleArea.getHeight()-1)))) {
			System.out.println("Selected invalid coordinate");
			return;
		}
		
		JsonObjectBuilder tileInfo = getTileInfo(selectedCoordinate);
		JsonObjectBuilder unitInfo = getUnitInfo(selectedCoordinate, session);
		
		JsonObjectBuilder builder = Json.createObjectBuilder().add("message_id", "user_selection_response").add("x", x).add("y", y);
		builder.add("tile_info", tileInfo.build());
		if(unitInfo != null) {
			builder.add("unit_info", unitInfo.build());
		}
		
		ServerSocket.sendJsonObject(session, builder.build());
	}
	
	private void handleTurnChange() {
		
		// swap active with waiting
		User wasActiveUser = this.activeUser;
		this.activeUser = this.waitingUser;
		this.waitingUser = wasActiveUser;
		
		this.activeUser.getArmy().resetUnitMovesAndAttacks();
		
		ServerSocket.sendJsonObject(this.activeUser.getSession(), Json.createObjectBuilder()
				.add("message_id", "start_turn")
				.build());
		ServerSocket.sendJsonObject(this.waitingUser.getSession(), Json.createObjectBuilder()
				.add("message_id", "end_turn")
				.build());
		
		updateAllUnits();
	}
	
	private void handleChat(String chatContents, Session session) {
		User u = getUserBySession(session);
		if (u == null) {
			return;
		}
		
		JsonObject chatMessage = Json.createObjectBuilder()
				.add("message_id",  "chat_message")
		        .add("player_name", u.getUsername())
				.add("chat_contents", chatContents)
				.build();
		
		ServerSocket.sendJsonObject(leftUser.getSession(), chatMessage);
		ServerSocket.sendJsonObject(rightUser.getSession(), chatMessage);
	}
	
	private void sendBattleOverMessage(User winner, User loser) {
		if(winner != null) {
			JsonObject winnerMessage = Json.createObjectBuilder()
					.add("message_id", "battle_over")
					.add("winner", true)
					.add("losing_player_name", loser.getUsername())
					.build();
			ServerSocket.sendJsonObject(winner.getSession(), winnerMessage);
		}
		
		if(loser != null) {
			JsonObject loserMessage = Json.createObjectBuilder()
					.add("message_id", "battle_over")
					.add("winner", false)
					.add("winning_player_name", winner.getUsername())
					.build();
			ServerSocket.sendJsonObject(loser.getSession(), loserMessage);
		}
	}

	private void sendInitialUnitMessage(User user, boolean isLeftSide) {
		
		int y = 0;
		int x;
		if (isLeftSide) {
			x = 0;
		} else {
			 x = battleArea.getWidth()-1;
		}
		
		// place units in the user's spawn zone
		for (Unit u : user.getArmy().getUnits()) {
			u.move(new Coordinate(x, y));
			y++;
			
			if (y == battleArea.getHeight()) {
				y = 0;
				if (isLeftSide) {
					x++;
				} else {
					x--;
				}
			}
		}
		
		updateOwnUnits(user);
	}

	private void sendMove(Unit u, Move m) {
		JsonObject moveMessage = Json.createObjectBuilder()
				.add("message_id", "unit_move")
				.add("unit", u.toJsonObject())
				.add("move", m.toJsonObject())
				.build();
		ServerSocket.sendJsonObject(leftUser.getSession(), moveMessage);
		ServerSocket.sendJsonObject(rightUser.getSession(), moveMessage);
	}

	private void startBattle() {
		leftUser.getArmy().resetUnitMovesAndAttacks();
		rightUser.getArmy().resetUnitMovesAndAttacks();
		updateAllUnits();
		this.battleState = BattleState.IN_PROGRESS;
		this.activeUser = leftUser;
		this.waitingUser = rightUser;
		handleTurnChange();
	}

	private void updateAllUnits() {
		
		User winner = null;
		User loser = null;
		
		Army leftArmy = leftUser.getArmy();
		Army rightArmy = rightUser.getArmy();
		JsonArray leftUnits = leftArmy.getUnitsOnMapAsJsonArray();
		JsonArray rightUnits = rightArmy.getUnitsOnMapAsJsonArray();
		
		JsonObjectBuilder leftMapUnitMessage = Json.createObjectBuilder()
				.add("message_id", "update_units")
				.add("own_units", leftUnits)
				.add("enemy_units", rightUnits)
				.add("number_of_actions_left", (MAX_ATTACKS_PER_TURN - leftArmy.getNumberOfActionsUsed()))
				.add("number_of_moves_left", (MAX_MOVES_PER_TURN - leftArmy.getNumberOfMovesUsed()));
		JsonObjectBuilder rightMapUnitMessage = Json.createObjectBuilder()
				.add("message_id", "update_units")
				.add("own_units", rightUnits)
				.add("enemy_units", leftUnits)
				.add("number_of_actions_left", (MAX_ATTACKS_PER_TURN - rightArmy.getNumberOfActionsUsed()))
				.add("number_of_moves_left", (MAX_MOVES_PER_TURN - rightArmy.getNumberOfMovesUsed()));
		ServerSocket.sendJsonObject(leftUser.getSession(), leftMapUnitMessage.build());
		ServerSocket.sendJsonObject(rightUser.getSession(), rightMapUnitMessage.build());
		
	
		
		if (leftUser.getArmy().hasLost()) {
			winner = rightUser;
			loser = leftUser;
		}else if(rightUser.getArmy().hasLost()) {
			winner = leftUser;
			loser = rightUser;
		}
		
		sendBattleOverMessage(winner, loser);
	}

	private void updateBattleOverview() {
		JsonObject battleOverviewMessage = Json.createObjectBuilder()
				.add("message_id",  "battle_overview")
		        .add("left_user", leftUser.getUserAsJsonObject())
				.add("right_user", rightUser.getUserAsJsonObject())
				.add("chances", getOverallChances())
				.build();
		
		ServerSocket.sendJsonObject(leftUser.getSession(), battleOverviewMessage);
		ServerSocket.sendJsonObject(rightUser.getSession(), battleOverviewMessage);
	}
	
	private void updateOwnUnits(User user) {
		JsonArray units = user.getArmy().getUnitsOnMapAsJsonArray();
		ServerSocket.sendJsonObject(user.getSession(), Json.createObjectBuilder()
				.add("message_id", "update_units")
				.add("own_units", units)
				.build());
	}
}
