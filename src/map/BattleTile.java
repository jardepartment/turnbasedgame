package map;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BattleTile extends Tile{
	
	public final static Set<String> BATTLE_TILE_TYPES = new HashSet<String>(Arrays.asList("grass", "trees", "sand", "rock", "water"));
	
	private int elevation;
	
	public BattleTile(String type, int elevation){
		super(type);
		this.elevation = elevation;
	}
	
	public int getElevation(){
		return this.elevation;
	}
	
	@Override
	public String toString() {
		return this.type.toUpperCase().charAt(0) + "["+this.elevation+"]";
	}

}
