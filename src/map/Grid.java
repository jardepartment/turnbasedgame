package map;

import java.util.HashSet;
import java.util.Set;

import javax.json.JsonObject;

public class Grid<T extends Tile> {

	public final static int TILE_AREA_SMALL_LB = 0;
	public final static int TILE_AREA_SMALL_UB = 13*9;
	
	public final static int TILE_AREA_MEDIUM_LB = 13*8;
	public final static int TILE_AREA_MEDIUM_UB = 16*11;
	
	public final static int TILE_AREA_LARGE_LB = 15*10;
	public final static int TILE_AREA_LARGE_UB = 21*15;
	
	public final static int TILE_AREA_X_LARGE_LB = 20*15;
	public final static int TILE_AREA_X_LARGE_UB = 50*50;//TODO max size?
	
	protected int gridHeight;
	protected int gridWidth;
	protected T[][] tiles;
	protected JsonObject mapJson;
	private Coordinate objectiveCoordinate;
	
	public Grid(JsonObject mapJson) {
		this.mapJson = mapJson;
	    this.gridHeight = mapJson.getInt("height");
		this.gridWidth = mapJson.getInt("width");
		
		int objX = mapJson.getInt("objective_coordinate_x");
		int objY = mapJson.getInt("objective_coordinate_y");
		objectiveCoordinate = new Coordinate(objX, objY);
		int numberOfTiles = mapJson.getJsonArray("tiles").size();
		
		if((this.gridHeight <= 1) || (this.gridWidth <= 1) ||
		   ((this.gridHeight*this.gridWidth) != numberOfTiles)) {
			throw new RuntimeException("invalid map dimensions");
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof Grid)) {
			return false;
		}
		
		Grid<?> g = (Grid<?>)o;
		
		return ((this.gridWidth == g.gridWidth) && 
				(this.gridHeight == g.gridHeight) && 
				(this.objectiveCoordinate.equals(g.objectiveCoordinate)) && 
				(this.mapJson.getJsonArray("tiles").toString().equals(g.mapJson.getJsonArray("tiles").toString())));
		
	}

	public Set<Coordinate> getAdjacentCoordinates(Coordinate current){
		
		Set<Coordinate> adjacentCoordinates = new HashSet<Coordinate>();
		
		boolean leftEdge = (current.x == 0);
		boolean rightEdge = (current.x == this.gridWidth-1);
		boolean topEdge = (current.y == 0);
		boolean bottomEdge = (current.y == this.gridHeight-1);
		
		if(!leftEdge && !topEdge) {
			adjacentCoordinates.add(new Coordinate(current.x-1, current.y-1));
		}
		
		if(!topEdge) {
			adjacentCoordinates.add(new Coordinate(current.x, current.y-1));
		}
		
		if(!rightEdge && !topEdge) {
			adjacentCoordinates.add(new Coordinate(current.x+1, current.y-1));
		}
		
		if(!leftEdge) {
			adjacentCoordinates.add(new Coordinate(current.x-1, current.y));
		}
		
		if(!rightEdge) {
			adjacentCoordinates.add(new Coordinate(current.x+1, current.y));
		}
		
		if(!leftEdge && !bottomEdge) {
			adjacentCoordinates.add(new Coordinate(current.x-1, current.y+1));
		}
		
		if(!bottomEdge) {
			adjacentCoordinates.add(new Coordinate(current.x, current.y+1));
		}
		
		if(!rightEdge && !bottomEdge) {
			adjacentCoordinates.add(new Coordinate(current.x+1, current.y+1));
		}
		
		return adjacentCoordinates;
	}

	public int getHeight() {
		return this.gridHeight;
	}

	public JsonObject getMapAsJson() {
		return this.mapJson;
	}

	public Coordinate getObjectiveCoordinate() {
		return objectiveCoordinate;
	}

	public T getTile(Coordinate c) {
		return this.tiles[c.x][c.y];
	}

	public int getWidth() {
		return this.gridWidth;
	}
	
	@Override
	public int hashCode() {
		//TODO
		return objectiveCoordinate.hashCode();
	}

	public void printGrid() {
		System.out.println("WIDTH:  " + this.gridWidth);
		System.out.println("HEIGHT: " + this.gridHeight);
		
		for(int row = 0; row < this.gridHeight; row++) {
			for(int column = 0; column < this.gridWidth; column++) {
				System.out.print(tiles[column][row].toString() + " ");
			}
			System.out.println("");
		}
	}
}
