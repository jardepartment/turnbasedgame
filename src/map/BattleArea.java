package map;

import java.util.HashSet;
import java.util.Set;

import javax.json.JsonArray;
import javax.json.JsonObject;

public class BattleArea extends Grid<BattleTile> {
	
	public static final int NUMBER_OF_SPAWN_COLUMNS = 2;
	
	public BattleArea(JsonObject mapJson) {
		super(mapJson);
		createBattleTileArray(mapJson.getJsonArray("tiles"));
	}
	
	public int getElevationDelta(BattleTile from, BattleTile to) {
		int elevationFrom = from.getElevation();
		int elevationTo = to.getElevation();
		return (elevationFrom - elevationTo);
	}

	public int getElevationDelta(Coordinate from, Coordinate to) {
		int elevationFrom = this.getTile(from).getElevation();
		int elevationTo = this.getTile(to).getElevation();
		return (elevationFrom - elevationTo);
	}
	
	public Set<Coordinate> getPossibleSpawnCoordinates(boolean isLeftSide){
		// TODO should it only be left vs right? why not top vs bottom? or corner vs corner? etc
		int startingColumn = isLeftSide ? 0 : this.gridWidth-NUMBER_OF_SPAWN_COLUMNS;
		Set<Coordinate> possibleSpawnCoordinates = new HashSet<Coordinate>();
		
		for(int i = startingColumn; i < (startingColumn+NUMBER_OF_SPAWN_COLUMNS); i++) {
			for(int j = 0; j < this.gridHeight; j++) {
				possibleSpawnCoordinates.add(new Coordinate(i, j));
			}
		}
		
		return possibleSpawnCoordinates;
	}

	private void createBattleTileArray(JsonArray tileJsonArray) {
		this.tiles = new BattleTile[this.gridWidth][this.gridHeight];
		for(int i = 0; i < tileJsonArray.size(); i++){
			Coordinate currentCoordinate = new Coordinate((i%this.gridWidth), (i/this.gridWidth));
			BattleTile tile;
			JsonObject tileJson = tileJsonArray.getJsonObject(i);
			int elevation = tileJson.getInt("elev");
			String type = tileJson.getString("type");
			if(BattleTile.BATTLE_TILE_TYPES.contains(type)) {
				tile = new BattleTile(type, elevation);
			} else {
				throw new RuntimeException(type + " is an invalid tile type in JSON mapfile");
			}
			
			this.tiles[currentCoordinate.x][currentCoordinate.y] = tile;
		}
	}
}
