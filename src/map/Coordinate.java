package map;

import javax.json.Json;
import javax.json.JsonObject;

public class Coordinate {
	
	public int x;
	public int y;
	
	public Coordinate() {
		this.x = -1;
		this.y = -1;
	}
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof Coordinate)) {
			return false;
		}
		
		Coordinate c = (Coordinate)o;
		
		return ((this.x == c.x) && (this.y == c.y));
		
	}

	public double getDistanceTo(Coordinate c) {
		return Math.sqrt(Math.pow((this.x-c.x), 2) + Math.pow((this.y-c.y), 2));
	}
	
	@Override
	public int hashCode() {
		return x << 16 | y;
	}

	public boolean isValid(Coordinate upperBounds) {
		return !((this.x < 0) || (this.y < 0) || 
				(this.x > upperBounds.x) || (this.y > upperBounds.y));
	}
	
	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
	
	public JsonObject toJsonObject() {
		return Json.createObjectBuilder().add("x", this.x).add("y", this.y).build();
	}

}
