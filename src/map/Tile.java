package map;

public abstract class Tile {

	protected String type;
	
	public Tile(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}
	
	public abstract String toString();

}
