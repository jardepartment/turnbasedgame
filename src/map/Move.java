package map;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

public class Move{
	double totalDistance;
	private Coordinate finalLocation;
	private List<Coordinate> steps;
	
	public Move(double totalDistance, Coordinate finalLocation, List<Coordinate> steps) {
		this.totalDistance = totalDistance;
		this.finalLocation = finalLocation;
		this.steps = steps;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		
		if(!(o instanceof Move)) {
			return false;
		}
		
		Move m = (Move)o;
		
		return ((this.finalLocation.equals(m.finalLocation)) && (this.totalDistance == m.totalDistance));
	}

	public double getDistance() {
		return this.totalDistance;
	}

	public Coordinate getFinalLocation() {
		return this.finalLocation;
	}

	public List<Coordinate> getSteps(){
		return this.steps;
	}
	
	@Override
	public int hashCode() {
		return this.finalLocation.hashCode();
	}

	public boolean isEqualAndBetterThan(Move m) {
		if ((this.finalLocation.equals(m.finalLocation)) &&
			(this.totalDistance < m.totalDistance)) {
			return true;
		}
		
		return false;
	}

	public void setSteps(List<Coordinate> steps) {
		this.steps = steps;
	}
	
	public JsonObject toJsonObject() {
		JsonArrayBuilder steps = Json.createArrayBuilder();
		for (Coordinate c : this.steps) {
			steps.add(c.toJsonObject());
		}
		
		return Json.createObjectBuilder()
				.add("final_location", this.finalLocation.toJsonObject())
				.add("steps", steps.build()).build();
	}
}